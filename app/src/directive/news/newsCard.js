'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */
module.exports = [
  function() {
    return {
      restrict: 'E',
      scope: {
        title: '=',
        data: '=',
        big: '='
      },
      templateUrl: 'templates/directiveViews/news/newsCard.html',
      controller: ['$scope','$rootScope', '$sce','$ionicPlatform', function($scope, $rootScope, $sce,$ionicPlatform){

        function imageFound() {
          // $scope.$apply(function(){
          //   $scope.data.goodImage = $scope.data.ogImage;
          //   console.log('image error');
          // });
        }

        function imageNotFound() {
          $scope.$apply(function(){
            $scope.data.ogImage = null;
          });
        }

        function testImage(URL) {
            var tester=new Image();
            tester.onload=imageFound;
            tester.onerror=imageNotFound;
            tester.src=URL;
        }

        // $ionicPlatform.ready(function() {
          $scope.baseUrl = $rootScope.baseUrl;
          if($scope.data && $scope.data.ogImage){
              $scope.data.ogImage = $sce.trustAsResourceUrl($scope.data.ogImage);
              testImage($scope.data.ogImage);
          }
        // });
        // console.log('controller');

      }],
      link: function(scope, element, attrs) {

        // scope.baseUrl = $rootScope.baseUrl;
        // if(scope.data && scope.data.ogImage){
        //     scope.data.ogImage = $sce.trustAsResourceUrl(scope.data.ogImage);
        // }
        // console.log('link');


      }
    };
  }
];
