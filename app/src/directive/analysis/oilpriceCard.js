'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/analysis/oilpriceCard.html',
            restrict: 'E',
            scope: {
              title: '@',
              header1: '=',  header2: '=', header3: '=',
              row1col1: '=', row2: '=', row8: '=',
              row2col1: '=', row3: '=', row9: '=',
              row3col1: '=', row5: '=', row11: '=',
              row4col1: '=', row6: '=', row12: '=',
              row1: '=',row4:'=', row7:'=',row10:'=',
              row2: '=',row5:'=', row8:'=',row11:'=',
              row3: '=',row6:'=', row9:'=',row12:'='          
            },
            link: function(scope, element, attrs) {
                scope.$watch('header2',function(newVal){
                  scope.header2 = newVal;
                });
                

                
                scope.$watch('row2', function(val){
                    if(scope.row2 && !isNaN(scope.row2)){
                        scope.row2 = parseFloat(scope.row2).toFixed(2);
                    }
                });
                scope.$watch('row3', function(val){
                    if(scope.row3 && !isNaN(scope.row3)){
                        scope.row3 = parseFloat(scope.row3).toFixed(2);
                    }
                });
                scope.$watch('row5', function(val){
                    if(scope.row5 && !isNaN(scope.row5)){
                        scope.row5 = parseFloat(scope.row5).toFixed(2);
                    }
                });
                scope.$watch('row6', function(val){
                    if(scope.row6 && !isNaN(scope.row6)){
                        scope.row6 = parseFloat(scope.row6).toFixed(2);
                    }
                });

                scope.$watch('row8', function(val){
                    if(scope.row8 && !isNaN(scope.row8)){
                        scope.row8 = parseFloat(scope.row8).toFixed(2);
                    }
                });
                scope.$watch('row9', function(val){
                    if(scope.row9 && !isNaN(scope.row9)){
                        scope.row9 = parseFloat(scope.row9).toFixed(2);
                    }
                });
                scope.$watch('row11', function(val){
                    if(scope.row11 && !isNaN(scope.row11)){
                        scope.row11 = parseFloat(scope.row11).toFixed(2);
                    }
                });
                scope.$watch('row12', function(val){
                    if(scope.row12 && !isNaN(scope.row12)){
                        scope.row12 = parseFloat(scope.row12).toFixed(2);
                    }
                });

            }
        };
    }
];

