'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/analysis/cardWithTable.html',
            restrict: 'E',
            scope: {
              title: '@',
              header1: '=',  header2: '=', header3: '=',
              row1col1: '=', row1col2: '=', row1col3: '=',
              row2col1: '=', row2col2: '=', row2col3: '=',
              row3col1: '=', row3col2: '=', row3col3: '=',
              row4col1: '=', row4col2: '=', row4col3: '=',
              row1: '=',row4:'=', row7:'=',
              row2: '=',row5:'=', row8:'=',
              row3: '=',row6:'=', row9:'='          
            },
            link: function(scope, element, attrs) {
                scope.$watch('header2',function(newVal){
                  scope.header2 = newVal;
                });


                scope.$watch('row2', function(val){
                    if(scope.row2 && !isNaN(scope.row2)){
                        scope.row2 = parseFloat(scope.row2).toFixed(2);
                    }
                });
                scope.$watch('row3', function(val){
                    if(scope.row3 && !isNaN(scope.row3)){
                        scope.row3 = parseFloat(scope.row3).toFixed(2);
                    }
                });
                scope.$watch('row5', function(val){
                    if(scope.row5 && !isNaN(scope.row5)){
                        scope.row5 = parseFloat(scope.row5).toFixed(2);
                    }
                });
                scope.$watch('row6', function(val){
                    if(scope.row6 && !isNaN(scope.row6)){
                        scope.row6 = parseFloat(scope.row6).toFixed(2);
                    }
                });
                scope.$watch('row8', function(val){
                    if(scope.row8 && !isNaN(scope.row8)){
                        scope.row8 = parseFloat(scope.row8).toFixed(2);
                    }
                });
                scope.$watch('row9', function(val){
                    if(scope.row9 && !isNaN(scope.row9)){
                        scope.row9 = parseFloat(scope.row9).toFixed(2);
                    }
                });

                scope.$watch('row1col2', function(val){
                    if(scope.row1col2 && !isNaN(scope.row1col2)){
                        scope.row1col2 = parseFloat(scope.row1col2).toFixed(2);
                    }
                });
                scope.$watch('row2col2', function(val){
                    if(scope.row2col2 && !isNaN(scope.row2col2)){
                        scope.row2col2 = parseFloat(scope.row2col2).toFixed(2);
                    }
                });
                scope.$watch('row3col2', function(val){
                    if(scope.row3col2 && !isNaN(scope.row3col2)){
                        scope.row3col2 = parseFloat(scope.row3col2).toFixed(2);
                    }
                });
                scope.$watch('row4col2', function(val){
                    if(scope.row4col2 && !isNaN(scope.row4col2)){
                        scope.row4col2 = parseFloat(scope.row4col2).toFixed(2);
                    }
                });

                scope.$watch('row1col3', function(val){
                    if(scope.row1col3 && !isNaN(scope.row1col3)){
                        scope.row1col3 = parseFloat(scope.row1col3).toFixed(2);
                    }
                });
                scope.$watch('row2col3', function(val){
                    if(scope.row2col3 && !isNaN(scope.row2col3)){
                        scope.row2col3 = parseFloat(scope.row2col3).toFixed(2);
                    }
                });
                scope.$watch('row3col3', function(val){
                    if(scope.row3col3 && !isNaN(scope.row3col3)){
                        scope.row3col3 = parseFloat(scope.row3col3).toFixed(2);
                    }
                });
                scope.$watch('row4col3', function(val){
                    if(scope.row4col3 && !isNaN(scope.row4col3)){
                        scope.row4col3 = parseFloat(scope.row4col3).toFixed(2);
                    }
                });

            }
        };
    }
];
