'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/doubleCards.html',
            restrict: 'E',
            scope: {
              title: '=',
              content: '=',
              timeago: '=',
              stripe: '=',
              resource: '=',
              sound: '=',
              thumbnail: '='
            },
            link: function(scope, elem, attr){

              scope.serverUrl = JSON.parse(localStorage.getItem('server')).url

              switch (scope.resource) {
                case "analyses": scope.tagName = "DAILY"
                case "weeklyAnalyses": scope.tagName = "WEEKLY"
                case "specialAnalyses": scope.tagName = "SPECIAL"
              }
              
              if(scope.resource === 'analyses'){
                scope.titlePlayer = 'Daily Analysis ' + moment(scope.timeago).format('DD MMM YYYY');
                scope.tagName = "DAILY"
              }
              else if (scope.resource === 'weeklyAnalyses'){
                scope.titlePlayer = 'Weekly Analysis ' + moment(scope.timeago).format('DD MMM YYYY');
                scope.tagName = "WEEKLY"
              }
              else if (scope.resource === 'specialAnalyses'){
                scope.tagName = "SPECIAL"
              }
              else {
                scope.tagName = "N/A"
              }

            }
        };
    }
];
