'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:SettingsController
 * @description
 * # SettingsController
 */
module.exports = [
  '$scope', '$ionicHistory', '$translate', '$rootScope','$cordovaDevice','allService','$ionicPlatform','$window','$state','$location',

  function($scope, $ionicHistory, $translate, $rootScope, $cordovaDevice,allService, $ionicPlatform, $window,$state,$location)
    {
      $ionicPlatform.ready(function() {

        $scope.$on("$ionicView.beforeEnter", function(event, data){
          if(ionic.Platform.isWebView()){
            screen.lockOrientation('portrait');
            console.log('lock portrait');
          }
        });

        $scope.selectLanguage = function(language) {
          $translate.use(language);
          $scope.language = language;
          localStorage.setItem("language", JSON.stringify(language));
          moment.locale(language);
          $ionicHistory.clearCache();
        };

        if($translate.use() == 'th'){
          $scope.language = 'th';
        }else {
          $scope.language = 'en';
        }

        $scope.goBack = function() {
          $ionicHistory.goBack();
        };

        $scope.base = {};
        $scope.base.ip = $rootScope.ip;
        $scope.newIp = null;

        $scope.selectIP = function(ip){
          $scope.base.ip = ip;
          $rootScope.ip = ip;
          $rootScope.baseUrl = "http://" + $rootScope.ip + ':8080';
          console.log($rootScope.baseUrl);
        };

        function reloadNotification(type){

          if(type == 'NEWS'){
              $scope.news = JSON.parse(localStorage.getItem(type));
              if($scope.news && $scope.news.id){
                $scope.news.checked = true;
              }else{
                $scope.news = {};
                $scope.news.checked = false;
                $scope.news.type = type;
              }
          }
          else if(type == 'ANALYSIS'){
              $scope.analysis = JSON.parse(localStorage.getItem(type));
              if($scope.analysis && $scope.analysis.id){
                $scope.analysis.checked = true;
              }else{
                $scope.analysis = {};
                $scope.analysis.checked = false;
                $scope.analysis.type = type;
              }
          }
          else if(type == 'BREAKING_NEWS'){
              $scope.breakingNews = JSON.parse(localStorage.getItem(type));
              if($scope.breakingNews && $scope.breakingNews.id){
                $scope.breakingNews.checked = true;
              }else{
                $scope.breakingNews = {};
                $scope.breakingNews.checked = false;
                $scope.breakingNews.type = type;
              }
          }
          else if(type == 'ECONOMIC_CALENDAR'){
              $scope.economicCalendar = JSON.parse(localStorage.getItem(type));
              if($scope.economicCalendar && $scope.economicCalendar.id){
                $scope.economicCalendar.checked = true;
              }else{
                $scope.economicCalendar = {};
                $scope.economicCalendar.checked = false;
                $scope.economicCalendar.type = type;
              }
          }

        }

        var registerId = localStorage.getItem('registerId');
        var deviceType = localStorage.getItem('deviceType');
        reloadNotification('NEWS');
        reloadNotification('BREAKING_NEWS');
        reloadNotification('ANALYSIS');
        reloadNotification('ECONOMIC_CALENDAR');

        $scope.updateNotification = function(noti){

          if(noti.checked){

            allService.addNotification({
              deviceType: deviceType,
              deviceId: registerId,
              type: noti.type
            })
            .then(function successCallback(result) {
                console.log('add Notification success:', result);
                localStorage.setItem(result.data.type, JSON.stringify(result.data));
                reloadNotification(result.data.type);

            }, function errorCallback(response) {
                console.log("Error cannot add notification:", response);
            });

          }else{

            allService.removeNotification({
              deviceType: deviceType,
              deviceId: registerId,
              type: noti.type,
              id: noti.id
            })
            .then(function successCallback(result) {
                localStorage.removeItem(noti.type);
                reloadNotification(noti.type);

            }, function errorCallback(response) {
                console.log("Error cannot remove notification:", response);
            });

          }

        };

        console.log("$rootScope.baseUrl:",$rootScope.baseUrl);

      });


    }

];
