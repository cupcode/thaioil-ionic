'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope', '$window', '$ionicHistory', '$rootScope', '$ionicModal', '$ionicLoading', '$cordovaGeolocation',
  '$timeout', 'allService', '$stateParams', '$cordovaDialogs', '$ionicPlatform', '$translate', '$cordovaActionSheet',
  'NgMap', '$q',

  function ($scope, $window, $ionicHistory, $rootScope, $ionicModal, $ionicLoading, $cordovaGeolocation, $timeout,
    allService, $stateParams, $cordovaDialogs, $ionicPlatform, $translate, $cordovaActionSheet, NgMap, $q) {

    $ionicPlatform.ready(function () {

      $scope.deviceType = localStorage.getItem('deviceType');
      $scope.server = JSON.parse(localStorage.getItem('server'));

      $scope.params = {};
      function prepareParamTypes() {
        console.log('prepareParamTypes')
        types = [];
        $scope.sources.forEach(function (src) {
          if (src.checked) {
            types.push(src.name);
          }
        });
        $scope.params.types = types.join();
        findByBounds();
      }
      $scope.$on("$ionicView.afterEnter", function (event, data) {
        console.log("afterEnter..")
        $scope.sources = []; //all active petrols to display in filter modal
        $q.all([
          allService.search("petrolStationTypes", "findAllPetrolStation", ""),
          allService.getActivePetrolStations()
        ]).then(function successCallback(response) {
          $scope.orderIndex = response[0].data._embedded.petrolStationTypes;
          response[1].data && response[1].data.forEach(function (src) {
            var index = _.findIndex($scope.orderIndex, function (o) {
              if (o.ref.toLowerCase() === 'pt' && src.toLowerCase() === 'ptg') {
                return true;
              } else if (o.ref.toLowerCase() === 'chevron' && src.toLowerCase() === 'caltex') {
                return true;
              } else {
                return o.ref.toLowerCase() === src.toLowerCase();
              }
            });
            var obj = {};
            obj.name = src;
            obj.checked = true;
            obj.src = src.toLowerCase() + '.jpg';
            obj.icon = 'images/pinGas/icon-marker-' + src.toLowerCase() + '.png';
            obj.index = $scope.orderIndex[index].orderPetrolStation || 999;
            $scope.sources.push(obj);
          });
          //need to be in order
          $scope.sources = _.sortBy($scope.sources, function (source) {
            return source.index;
          });
          prepareParamTypes();
        },
          function errorCallback(response) {
            console.log('Error list resource=' + $scope.resource, response);
          });
        // screen.unlockOrientation();
      });

      $scope.promotionExist = $rootScope.promotionExist;
      $rootScope.$on('promotionExist', function (events, promotion) {
        $scope.promotionExist = promotion.promotion > 0;
      });


      $scope.goBack = function () {
        $ionicHistory.goBack();
      };

      if ($stateParams.station) {
        $scope.station = $stateParams.station;
        alert($scope.station);
      }

      $scope.location = {};
      $scope.markers = [];
      $scope.boundsExist = false;
      $timeout(function () {

        $scope.location.lat = 13.7563;
        $scope.location.lng = 100.5018;

        var latlng = new google.maps.LatLng($scope.location.lat, $scope.location.lng);
        var myOptions = {
          zoom: 5,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          minZoom: 8,
          maxZoom: 18
        };
        $scope.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        $scope.overlay = new google.maps.OverlayView();
        $scope.overlay.draw = function () { }; // empty function required
        $scope.overlay.setMap($scope.map);
        $scope.element = document.getElementById('map_canvas');

        // google.maps.event.addListener($scope.map, 'dragend', function(){
        //   console.log('moved!');
        //   findByBounds();
        // });
        //
        // google.maps.event.addListener($scope.map, 'zoom_changed', function(){
        //   console.log('zoomed!');
        //   findByBounds();
        // });

        google.maps.event.addListener($scope.map, 'bounds_changed', function () {
          console.log('bounds_changed!');
          $timeout.cancel($scope.delay);
          $scope.boundsExist = true;
          findByBounds();
        });

        //FOR THE FIRST TIME
        if (navigator.geolocation) {
          console.log('navigator.geolocation:', navigator.geolocation);
          var options = { maximumAge: 0, timeout: 100000, enableHighAccuracy: true };
          $ionicLoading.show();
          navigator.geolocation.getCurrentPosition(success, error, options);
        }
        else {

          $cordovaDialogs.alert($translate.instant('TURN_ON_GPS'), $translate.instant('CANNOT_RETRIEVE_LOCATION'), $translate.instant('CONFIRM'))
            .then(function () {

            });

        }
      }, 300);

      var currentPosition = {
        lat: 0,
        lng: 0
      };

      function success(position) {
        $ionicLoading.hide();

        currentPosition.lat = position.coords.latitude;
        currentPosition.lng = position.coords.longitude;

        var GeoMarker = new GeolocationMarker($scope.map);
        $scope.map.panTo(currentPosition);
        $scope.map.setZoom(15);
        console.log("success")
        findByBounds();
      }

      function error(error) {
        $ionicLoading.hide();

        $cordovaDialogs.alert($translate.instant('TURN_ON_GPS'), $translate.instant('CANNOT_RETRIEVE_LOCATION'), $translate.instant('CONFIRM'))
          .then(function () {

          });

      }

      //deleteAllMarkers();

      var tmpMarker;
      function findByBounds() {
        if (!$scope.boundsExist) {
          console.log("boundsNotExist!")
          return
        }
        console.log("findByBounds")
        $ionicLoading.show();

        $scope.resource = 'petrolStations';
        $scope.searchMethod = 'findByBoundsAndTypes';

        $scope.params.northEastLat = $scope.map.getBounds() && $scope.map.getBounds().getNorthEast().lat();
        $scope.params.northEastLng = $scope.map.getBounds() && $scope.map.getBounds().getNorthEast().lng();
        $scope.params.southWestLat = $scope.map.getBounds() && $scope.map.getBounds().getSouthWest().lat();
        $scope.params.southWestLng = $scope.map.getBounds() && $scope.map.getBounds().getSouthWest().lng();

        if (!$scope.params.types) {
          $scope.params.types = '%%';
        }

        allService.search($scope.resource, $scope.searchMethod, $scope.params)
          .then(function successCallback(response) {

            $scope.newMarkers = [];
            var newCount = 0;
            var oldCount = 0;
            var keepCount = 0;

            var petrolStations = response.data._embedded.petrolStations;

            if (petrolStations && petrolStations.length > 0) {
              console.log('before $scope.markers:', $scope.markers);
              petrolStations.forEach(function (petrol) {

                tmpMarker = Marker(petrol);
                var foundObj = ifExistInList(tmpMarker, $scope.markers); //if exist return marker, else null
                if (foundObj) {
                  $scope.newMarkers.push(foundObj); //keep the already exist one, but don't have to create it again
                } else {
                  tmpMarker.setMap($scope.map);
                  $scope.newMarkers.push(tmpMarker); //create new marker
                }

              });

              $scope.markers.forEach(function (marker) {
                var foundObj = ifExistInList(marker, $scope.newMarkers);
                if (!foundObj) { //remove old marker that no longer exist in current view
                  marker.setMap(null);
                }
              });
              // if ($scope.markerCluster)
              //   $scope.markerCluster.clearMarkers();

              $scope.markers = $scope.newMarkers;
              // console.log(' $scope.markers', $scope.markers)
              // $scope.markerCluster = new MarkerClusterer($scope.map, $scope.markers,
              //   { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });


            } else {
              deleteAllMarkers();
            }
            $ionicLoading.hide();

          }, function errorCallback(response) {
            console.log('Error list resource=' + $scope.resource, response);
          });

      }

      function ifExistInList(obj, list) {

        var found = null;
        for (var i = 0; i < list.length; i++) {
          if (list[i].title === obj.title) {
            found = list[i];
            break;
          }
        }

        return found;

      }

      function Marker(petrol) {

        var id;
        for (var i = 0; i <= $scope.sources.length; i++) {
          if ($scope.sources[i].name === petrol.type) {
            id = i;
            break;
          }
        }

        var marker = new google.maps.Marker({
          position: { lat: petrol.lat, lng: petrol.lon },
          title: petrol._links.self.href,
          icon: new google.maps.MarkerImage(
            $scope.sources[id].icon,
            null,//new google.maps.Size(30,60), //size
            null, //origin
            null, //anchor
            new google.maps.Size(40, 50) //scale
          ),
        });
        var options = {
          title: 'What do you want with this image?',
          buttonLabels: ['Share via Facebook', 'Share via Twitter'],
          addCancelButtonWithLabel: 'Cancel',
          androidEnableCancelButton: true,
          winphoneEnableCancelButton: true,
          addDestructiveButtonWithLabel: 'Delete it'
        };
        // var delay
        google.maps.event.addDomListener(marker, 'mousedown', function () {
          $scope.google_lat = this.getPosition().lat();
          $scope.google_lng = this.getPosition().lng();

          // delay = $timeout(goToGoogleMap(), 50000);
          google.maps.event.addListener($scope.map, 'zoom_changed', function () {
            console.log('Zoom map')
            $timeout.cancel($scope.delay);
          });

          $scope.delay = $timeout(function () {
            goToGoogleMap();
          }, 2000);

          google.maps.event.addListener($scope.map, 'zoom_changed', function () {
            $timeout.cancel($scope.delay);
          });



        });
        google.maps.event.addDomListener(marker, 'mouseup', function () {
          // console.log('mouseup Marker', $scope.delay);
          // clearTimeout(delay);

          $timeout.cancel($scope.delay);
        });
        return marker;
      }

      function goToGoogleMap() {
        $timeout.cancel($scope.delay);
        $cordovaActionSheet
          .show({
            title: 'Open with ...',
            buttonLabels: ['Google Maps'],
            addCancelButtonWithLabel: 'Cancel',
            androidEnableCancelButton: true,
            winphoneEnableCancelButton: true,
            androidTheme: window.plugins.actionsheet.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
            // addDestructiveButtonWithLabel : 'Delete it'
          })
          .then(function (btnIndex) {
            var index = btnIndex;
            if (btnIndex === 1) {
              window.open('https://www.google.com/maps/place/' + $scope.google_lat + ',' + $scope.google_lng, '_system')
            }
          });
      }

      function deleteAllMarkers() {
        $timeout(function () {
          $scope.markers.forEach(function (marker) {
            marker.setMap(null);
          });
          $scope.markers = [];
        }, 400);

      }
      //MODAL
      $scope.serverUrl = JSON.parse(localStorage.getItem('server')).url + '/gasStations/logo/'
      $ionicModal.fromTemplateUrl('templates/views/filter.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
      });

      $scope.showModal = function () {
        console.log('shoeModal')
        $scope.modal.show();
      };

      var types = [];
      $scope.closeModal = function () {
        $scope.modal.hide();
        prepareParamTypes();
      };

      $scope.checkAll = false;
      $scope.toggleCheckAll = function () {
        console.log($scope.checkAll);
        if ($scope.checkAll) {
          $scope.sources.forEach(function (src) {
            src.checked = true;
          });
        } else {
          $scope.sources.forEach(function (src) {
            src.checked = false;
          });
        }
        $scope.checkAll = !$scope.checkAll;
      };

      $scope.gotoCurrentLocation = function () {
        if (currentPosition.lat === 0 && currentPosition.lng === 0) {
          var options = { maximumAge: 0, timeout: 100000, enableHighAccuracy: true };
          $ionicLoading.show();
          navigator.geolocation.getCurrentPosition(success, error, options);
        } else {
          $scope.map.panTo(currentPosition);
          $scope.map.setZoom(15);
          console.log('gotoCurrentLocation')
          findByBounds();
        }

      };

      $scope.$on("$ionicView.beforeLeave", function (event, data) {
        $rootScope.hideTabs = false;
      });

      $scope.placeChanged = function () {
        $scope.place = this.getPlace();
        // console.log('place', $scope.place);
        var selectLocation = {
          lat: $scope.place.geometry.location.lat(),
          lng: $scope.place.geometry.location.lng()
        }
        $scope.map.panTo(selectLocation);
        $scope.map.setZoom(15);
        findByBounds();
        var marker = new google.maps.Marker({
          position: selectLocation,
        });
        marker.setMap($scope.map);

        // var delay
        google.maps.event.addDomListener(marker, 'mousedown', function () {
          $scope.google_lat = this.getPosition().lat()
          $scope.google_lng = this.getPosition().lng()

          console.log('mousedown placeChanged: ', $scope.google_lat, $scope.google_lng)

          // delay = $timeout(goToGoogleMap(), 5000)

          $scope.delay = $timeout(function () {
            console.log('update greeting placeChanged');
            goToGoogleMap();
          }, 2000);



        });
        google.maps.event.addDomListener(marker, 'mouseup', function () {
          console.log('mouseup placeChanged', $scope.delay)
          // clearTimeout($scope.delay)
          $timeout.cancel($scope.delay);
        });

      };

    });
  }];
