'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */
module.exports = ['$sce',
  function ($sce) {
    return {
      restrict: 'E',
      scope: {
        title: '=',
        content: '=',
        timeago: '=',
        stripe: '='
      },
      templateUrl: 'templates/directiveViews/singleCard.html',
      link: function (scope, elem, attr) {
        scope.trust = function (html_code) {
          return $sce.trustAsHtml(html_code);
        };
      }
    };
  }
];
