'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */

module.exports = ['$scope', '$state', 'allService', '$location', '$rootScope', '$ionicHistory', '$q',

  function ($scope, $state, allService, $location, $rootScope, $ionicHistory, $q) {
    $scope.$on("$ionicView.afterEnter", function (event, data) {
      console.log('afterEnter gasStationPrice unlock');
      screen.unlockOrientation();
    });
    $scope.$on('orientationchange', function (event, args) {
      if ((args === 'landscape-primary' || args === 'landscape-secondary') && $location.path().indexOf('gasStationPrice') > -1) {
        $state.go('retailPriceLandscape', { item: $scope.items, tax: $scope.tax });
      }
    });
    $scope.serverUrl = JSON.parse(localStorage.getItem('server')).url + '/gasStations/logo/';
    $scope.resource = 'retailPrices';
    $scope.searchMethod = 'findLastestRetailPrice';
    $scope.temp = [];
    $scope.getGasStaionPriceList = function () {
      $q.all([
        allService.search("petrolStationTypes", "findAllRetailPrice", ""),
        allService.search($scope.resource, $scope.searchMethod, ""),
        allService.getById("retailTaxes", 1),
      ]).then(function successCallback(response) {
        var orderTemp = response[0].data._embedded.petrolStationTypes;
        $scope.tax = parseFloat(response[2].data.tax);
        $scope.orderIndex = []
        var data = response[1].data;
        $scope.temp = data._embedded[$scope.resource];
        $scope.items = [];
        for (var i = 0; i < orderTemp.length; i++) {
          if (orderTemp[i].ref.toLowerCase() !== 'esso' && orderTemp[i].ref.toLowerCase() !== 'bangchak' && orderTemp[i].ref.toLowerCase() !== 'thaioil') {
            $scope.orderIndex[orderTemp[i].orderRetailPrice - 1] = orderTemp[i];
            for (var index = 0; index < $scope.temp.length; index++) {
              if (orderTemp[i].ref === $scope.temp[index].nameEn) {
                $scope.items[orderTemp[i].orderRetailPrice - 1] = $scope.temp[index];
              }
            }
          }
        }
        $scope.items = _.compact($scope.items);
        $scope.orderIndex = _.compact($scope.orderIndex);
        for (var index = 0; index < $scope.items.length; index++) {
          console.log('$scope.items', $scope.items[index]);
          var dataTemp = $scope.items[index];
          $scope.items[index].diesel = dataTemp.diesel !== null ? parseFloat(dataTemp.diesel) + $scope.tax : dataTemp.diesel;
          $scope.items[index].gasohol91 = dataTemp.gasohol91 !== null ? parseFloat(dataTemp.gasohol91) + $scope.tax : dataTemp.gasohol91;
          $scope.items[index].gasohol95 = dataTemp.gasohol95 !== null ? parseFloat(dataTemp.gasohol95) + $scope.tax : dataTemp.gasohol95;
          $scope.items[index].gasoholE20 = dataTemp.gasoholE20 !== null ? parseFloat(dataTemp.gasoholE20) + $scope.tax : dataTemp.gasoholE20;
          $scope.items[index].gasoholE85 = dataTemp.gasoholE85 !== null ? parseFloat(dataTemp.gasoholE85) + $scope.tax : dataTemp.gasoholE85;
          $scope.items[index].gasoline95 = dataTemp.gasoline95 !== null ? parseFloat(dataTemp.gasoline95) + $scope.tax : dataTemp.gasoline95;
          $scope.items[index].premiumDiesel = dataTemp.premiumDiesel !== null ? parseFloat(dataTemp.premiumDiesel) + $scope.tax : dataTemp.premiumDiesel;

        }
        console.log('$scope.items', $scope.items);
      }, function errorCallback(response) {
        console.log('Error search resource:' + $scope.resource + ',searchMethod:' + $scope.searchMethod, response);
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.getGasStaionPriceList();

    $scope.viewRetailPrice = function (gas) {
      var index = _.findIndex($scope.items, function (item) {
        return item.nameEn.toLowerCase() === gas
      });

      if ($location.path().indexOf('/home/gasStationPrice') > -1) {
        $state.go('tab.retailPricePortrait', { items: $scope.items, index: index, orderIndex: $scope.orderIndex, tax: $scope.tax });
      } else if ($location.path().indexOf('/more/gasStationPrice') > -1) {
        $state.go('tab.moreRetailPricePortrait', { items: $scope.items, index: index, orderIndex: $scope.orderIndex, tax: $scope.tax });
      }

    };

    $scope.goBack = function () {
      $ionicHistory.goBack();
    };

    $scope.openMap = function () {
      $state.go('tab.map');
    };

  }
];
