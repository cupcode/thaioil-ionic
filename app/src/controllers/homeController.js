'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope', '$timeout', 'allService', '$ionicScrollDelegate', '$window', 'moment', '$state', '$q', '$ionicPlatform', '$rootScope', '$ionicHistory', '$location', '$stateParams',

  function ($scope, $timeout, allService, $ionicScrollDelegate, $window, moment, $state, $q, $ionicPlatform, $rootScope, $ionicHistory, $location, $stateParams) {

    $ionicPlatform.ready(function () {
      
      $scope.resource = 'home';
      $scope.baseUrl = $rootScope.baseUrl;
    
      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (ionic.Platform.isWebView()) {
          screen.lockOrientation('portrait');
          console.log('lock portrait');
        }
      });
      $scope.$on("$ionicView.afterEnter", function (event, data) {
        $ionicHistory.clearHistory();
        console.log('$stateParams.goToCalendar:', $stateParams.goToCalendar, $rootScope.goToCalendar);
        if ($rootScope.goToCalendar) {
          $rootScope.goToCalendar = false;
          $state.go('tab.calendar');
        }
      });

      $scope.Height = $window.innerHeight - 140;

      // var breakingNews = 'breakingNews';
      // var analyses = 'analyses';
      // var news = 'news';
      var searchMethod = 'findAllAvailableNow';
      var params = {
        page: 0,
        size: 1,
        projection: 'mobileList',
        sort: 'publishDatetime,DESC'
      };

      $scope.breakingNewsToday = false;

      $scope.refresh = function () {

        $scope.items = [];

        $q.all([
          allService.search('breakingNews', searchMethod, params),
          allService.search('analyses', searchMethod, { page: 0, size: 2, projection: 'mobileList', sort: 'publishDatetime,DESC' }),
          allService.search('news', searchMethod, { page: 0, size: 7, projection: 'mobileList', sort: 'publishDatetime,DESC' }),
          allService.search('economicCalendars', 'findBetween', {
            'page': 0,
            'size': 4,
            'sort': 'publishDatetime,DESC',
            'projection': 'mobileList',
            'startDate': moment().subtract(1, 'd').format('YYYY-MM-DD') + ' 00:00:00', //-1day
            'endDate': moment().format('YYYY-MM-DD') + ' 23:59:59', //today
          }),
          allService.search('weeklyAnalyses', "findByTitle", { keyword: "%%", locale: 'TH', page: 0, size: 1, projection: 'mobileList', sort: 'publishDatetime,DESC' }),
          allService.search('specialAnalyses', "findByTitle", { keyword: "%%", locale: 'TH', page: 0, size: 1, projection: 'mobileList', sort: 'publishDatetime,DESC' }),
          allService.search('promotions', 'findBanner', { keyword: "%%", locale: 'TH', page: 0, size: 9999, projection: 'mobileList' }),
          allService.search('promotions', 'findAllAvaliable', { page: 0, size: 1, projection: 'mobileList' }),
          allService.getById("retailTaxes", 1),
        ]).then(function successCallback(results) {
          console.log('Home:', results);

          $scope.breakingNews = results[0].data._embedded.breakingNews;
          if ($scope.breakingNews && $scope.breakingNews.length > 0) {
            $scope.breakingNews = $scope.breakingNews[0];
            if (moment().format('YYYYMMDD') === moment($scope.breakingNews.publishDatetime).format('YYYYMMDD')) {
              $scope.breakingNewsToday = true;
              $rootScope.$broadcast('breakingNewsUpdate', $scope.breakingNews);
            } else {
              $scope.breakingNewsToday = false;
              $rootScope.$broadcast('breakingNewsUpdate', null);
            }
            $scope.loading = false;
          }
          $scope.latestAnalysis = results[1].data._embedded && results[1].data._embedded.analyses;
          $scope.latestNewses = results[2].data._embedded && results[2].data._embedded.news;

          //Calendar
          $scope.economicCalendars = results[3].data._embedded && results[3].data._embedded.economicCalendars;
          if (!$scope.economicCalendars) {
            getThis2WeekCalendar();
          } else {
            getCalendar();
          }

          //Analysis
          console.log("latestAnalysis: ", $scope.latestAnalysis)
          $scope.latestAnalysis = $scope.latestAnalysis.concat(results[4].data._embedded.weeklyAnalyses)
          var specialPublishDate = results[5].data._embedded.length > 0 && results[5].data._embedded.specialAnalyses[0].publishDatetime || null

          if ($scope.latestAnalysis && specialPublishDate && moment.duration(moment().diff(moment(specialPublishDate))).asDays() <= 7) {
            $scope.latestAnalysis = $scope.latestAnalysis.concat(results[5].data._embedded.specialAnalyses)
          } else {
            // console.log("special publishDate is too old", specialPublishDate, moment.duration(moment().diff(moment(specialPublishDate))).asDays())
          }

          $scope.latestAnalysis = $scope.resizeImage($scope.latestAnalysis)

          $scope.canLoad = false;
          $scope.$broadcast('scroll.refreshComplete');

          //Promotion
          $scope.promotions = results[6].data._embedded.promotions
          $scope.havePromotion = results[7].data.page.totalElements
          $rootScope.promotionExist = $scope.havePromotion > 0;
          $rootScope.$broadcast('promotionExist', { promotion: $scope.havePromotion });

          //Tax
          $scope.tax = parseFloat(results[8].data.tax);
          $rootScope.$broadcast('taxRefresh');

          $rootScope.calendarCount = 0;
          $rootScope.$broadcast('calendarCount');
          $rootScope.breakingNewsCount = 0;
          $rootScope.$broadcast('breakingNewsCount');

          $scope.canLoad = false;
          $scope.$broadcast('scroll.refreshComplete');
          

        }, function errorCallback(response) {
          $scope.canLoad = false;
          console.log('Error search home', response);
        });

      };

      function getLastWeekCalendar() {
        allService.search('economicCalendars', 'findBetween', {
          'page': 0,
          'size': 4,
          'sort': 'publishDatetime,DESC',
          'projection': 'mobileList',
          'startDate': moment().startOf('isoweek').subtract(7, 'd').format('YYYY-MM-DD') + ' 00:00:00', //start-of-lastweek-week
          'endDate': moment().endOf('isoweek').subtract(7, 'd').format('YYYY-MM-DD') + ' 23:59:59' //end-of-last-week
        }).then(function successCallback(results) {
          console.log('thisWeekCalendar:', results.data._embedded.economicCalendars);
          $scope.economicCalendars = results.data._embedded.economicCalendars;
          getCalendar();
        }, function errorCallback(response) {
          console.log('cannot get lastWeekCalendar', response);
        });
      }

      function getThis2WeekCalendar() {

        allService.search('economicCalendars', 'findBetween', {
          'page': 0,
          'size': 4,
          'sort': 'publishDatetime,ASC',
          'projection': 'mobileList',
          'startDate': moment().startOf('isoweek').format('YYYY-MM-DD') + ' 00:00:00', //start-of-this-week
          'endDate': moment().endOf('isoweek').add(7, 'd').format('YYYY-MM-DD') + ' 23:59:59' //end-of-next-week
        }).then(function successCallback(results) {
          console.log('thisWeekCalendar:', results.data._embedded.economicCalendars);
          $scope.economicCalendars = results.data._embedded.economicCalendars;
          if (!$scope.economicCalendars) {
            getLastWeekCalendar();
          } else {
            getCalendar();
          }
        }, function errorCallback(response) {
          console.log('cannot get thisWeekCalendar', response);
        });
      }

      $scope.scrollTopAndRefresh = function () {
        $ionicScrollDelegate.scrollTop('animate');
        $timeout(function () {
          $scope.refresh();
        }, 300);
      }
      function showBubble() {
        if ($rootScope.breakingNewsCount + $rootScope.calendarCount > 0) {
          $scope.showBubble = true;
        }
        else {
          $scope.showBubble = false;
        }
      }

      showBubble();

      $rootScope.$on('breakingNewsCount', function (events, count) {
        // console.log('$rootScope.$broadcast(breakingNewsCount', count)
        showBubble();
      });

      $rootScope.$on('calendarCount', function (events, count) {
        showBubble();
      });



      $rootScope.$on('breakingNewsUpdate', function (event, breakingNews) {
        // console.log('home receive breakingNewsUpdate');
        $scope.breakingNews = breakingNews;
        if ($scope.breakingNews) {
          $scope.breakingNewsToday = true;
          $scope.scrollTopAndRefresh = function () {
            $ionicScrollDelegate.scrollTop('animate');
            $timeout(function () {
              $scope.refresh();
            }, 300);
          }
        } else {
          $scope.breakingNewsToday = false;
        }
      });

      $rootScope.$on('online', function () {
        $scope.loading = true;
        $scope.refresh();
      });
      $rootScope.$on('refresh', function (event, location) {
        // console.log('refresh home: ', $location.path(), location);
        if ($location.path() === location) {
          if ($rootScope.calendarCount + $rootScope.breakingNewsCount > 0) {
            $scope.loading = true;
            $scope.refresh();
          }
        }
        else if (location === 'all') {
          $scope.loading = true;
          $scope.refresh();
        }
      });
      $scope.openMap = function () {
        $state.go('tab.map', {});
      };


      //Notification
      $rootScope.$on('registerId', function () {
        allService.registerNotification();
      });
      allService.registerNotification();

      if ($rootScope.welcome) {
        // in case: no welcome page
        var tutorial = JSON.parse(localStorage.getItem('tutorial'));
        if (!tutorial) {
          $state.go('tutorial');
        }
      } else {
        // Tutorial (after welcome page (if has))
        $rootScope.$on('welcomeModalClose', function () {
          var tutorial = JSON.parse(localStorage.getItem('tutorial'));
          if (!tutorial) {
            $state.go('tutorial');
          }
        })
      };

      function getCalendar() {
        // allService.allCalendarCountries()
        //   .then(function successCallback(response) {
        //     $scope.data_countries = response.data._embedded.countries;

        //     if ($scope.economicCalendars && $scope.economicCalendars.length > 0) {
        //       $scope.economicCalendars.forEach(function (economicCalendar) {
        //         economicCalendar.country = _.find($scope.data_countries, { 'id': economicCalendar.countryId });
        //       })
        //     }

        //   }, function errorCallback(response) {
        //     $scope.$broadcast('scroll.refreshComplete');
        //     console.log('Error allCalendarCountries', response);
        //   });
      }


      $scope.canLoad = true;
      $scope.loading = false;
      $scope.refresh();

      $scope.resizeImage = function (data) {
        for (var i = 0; i < data.length; i++) {
          console.log('data[i]', data[i])
          if (data[i] && data[i].thumbnailPath !== null) {
            var dataSplit = data[i].thumbnailPath.split('/')
            var result = ''
            for (var j = 0; j < dataSplit.length; j++) {
              if (j === dataSplit.length - 1) {
                result = result + '/resize_500_' + dataSplit[j]
              } else if (j === 0) {
                result = dataSplit[j]
              } else {
                result = result + '/' + dataSplit[j]
              }
            }
            data[i].thumbnailPath = result
          }
        }
        return data
      }

      $scope.redirectImage = function (data) {
        if (data && data.newsSourceName === 'TNN24') {
          var dataSplit = data.ogImage.split("/")
          var result = ''
          for (var i = 0; i < dataSplit.length; i++) {
            if (i === dataSplit.length - 1) {
              result = result + '/' + dataSplit[i]
            } else if (i === 0) {
              result = dataSplit[i]
            } else {
              result = result + '/' + dataSplit[i]
            }
          }
          return result
        } else {
          return 'images/img/no_image300x300.png'
        }
      }


    });
  }];
