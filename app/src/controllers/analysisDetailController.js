'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope', '$ionicHistory', '$stateParams', 'allService', '$state', '$sce', '$location', '$ionicModal', '$rootScope', '$ionicScrollDelegate', '$timeout',
  function ($scope, $ionicHistory, $stateParams, allService, $state, $sce, $location, $ionicModal, $rootScope, $ionicScrollDelegate, $timeout) {

    if (localStorage.fontSize) {
      $scope.fontSize = parseInt(localStorage.fontSize);
    } else {
      $scope.fontSize = $rootScope.fonts[0];
    }

    $scope.toggleFont = function () {
      var index = _.indexOf($rootScope.fonts, $scope.fontSize);
      if (index === $rootScope.fonts.length - 1) {
        index = -1;
      }
      $scope.fontSize = $rootScope.fonts[index + 1];
      localStorage.fontSize = $scope.fontSize;
      $rootScope.$broadcast('fontSizeChanged');
    }

    $scope.$on("$ionicView.afterEnter", function (event, data) {

      var historyId = $ionicHistory.currentHistoryId();
      console.log("add history:" + historyId, $ionicHistory.viewHistory().histories[historyId].stack);

      if (ionic.Platform.isWebView()) {
        console.log('afterEnter analysisDetail unlock');
        screen.unlockOrientation();
      }
    });

    $scope.$on('orientationchange', function (event, args) {
      if ((args === 'landscape-primary' || args === 'landscape-secondary') && $location.path().indexOf('analyses/detail') > -1) {
        $state.go('analysisDetailLandscape', { item: $scope.item, resource: $scope.resource });
      }
    });

    $scope.selectedTab = 1;
    $scope.selectTab = function (tabIndex) {
      $scope.selectedTab = tabIndex;
      $timeout(function () {
        $ionicScrollDelegate.resize();
        console.log('resize..')
      }, 100);
    };
    $scope.goBack = function () {
      $ionicHistory.goBack();
    };

    $scope.resource = $stateParams.resource;
    $scope.item = $stateParams.item;
    $scope.itemId = $stateParams.item.id;
    $scope.homeMenu = $stateParams.homeMenu;
    console.log('$stateParams.item:', $stateParams.item);

    if ($scope.resource === 'analyses') {
      $scope.titlePlayer = 'Daily Analysis ' + moment($scope.item.publishDatetime).format('DD MMM YYYY');
    } else {
      $scope.titlePlayer = 'Weekly Analysis ' + moment($scope.item.publishDatetime).format('DD MMM YYYY');
    }

    //------------------------ PREVIOUS ARTICLE ------------------------
    $scope.searchMethod = 'findSince';
    $scope.params = {
      'page': 0, //always be 0
      'size': 3,
      'sort': 'publishDatetime,DESC',
      'since': $scope.item.publishDatetime.replace('T', ' ').split('.')[0],
      'projection': 'mobileList'
    };
    $scope.prevArticles = [];

    allService.search($scope.resource, $scope.searchMethod, $scope.params)
      .then(function successCallback(response) {
        console.log('search prevArticles:', response);
        var data = response.data;
        if (data.page && data.page.number < data.page.totalPages) {
          $scope.prevArticles = data._embedded[$scope.resource];
        }

      }, function errorCallback(response) {
        console.log('Error list resource=' + $scope.resource, response);
      });

    ////------------------------ GET SELF DATA ------------------------
    $scope.loading = true;
    allService.getById($scope.resource, $scope.item.id)
      .then(function successCallback(response) {
        $scope.item = $scope.resizeImage(response.data);
        
        console.log('$scope.item:', $scope.item);
        $scope.loading = false;
      }, function errorCallback(response) {
        console.log('Error: getById ' + $scope.resource + '/' + $scope.item.id, response);
      });

    $scope.gotoAnalysisDetail = function (analysis) {

      if ($location.path().indexOf('home') > -1) {
        $state.go('tab.homeAnalysisDetail', { itemId: analysis.id, item: analysis, resource: $scope.resource });
      } else {
        $state.go('tab.analysisDetail', { itemId: analysis.id, item: analysis, resource: $scope.resource });
      }

    };

    $scope.trust = function (html_code) {
      return $sce.trustAsHtml(html_code);
    };


    $scope.defaultImage = setDefaultImage($scope.resource)
    $scope.baseUrl = $rootScope.baseUrl;


    function setDefaultImage(resource) {
      console.log('resource', resource)
      switch (resource) {
        case "analyses":
          return $scope.baseUrl + '/analysis/default/daily.png'
        case "weeklyAnalyses":
          return $scope.baseUrl + '/analysis/default/weekly.png'
        case "specialAnalyses":
          return $scope.baseUrl + '/analysis/default/special.png'
      }
    }

    $scope.resizeImage = function (data) {
      if (data.thumbnailPath !== null) {
        var dataSplit = data.thumbnailPath.split('/')
        var result = ''
        for (var j = 0; j < dataSplit.length; j++) {
          if (j === dataSplit.length - 1) {
            result = result + '/resize_500_' + dataSplit[j]
          } else if (j === 0) {
            result = dataSplit[j]
          } else {
            result = result + '/' + dataSplit[j]
          }
        }
        data.thumbnailPath = result
      }
      return data
    }
  }
];
