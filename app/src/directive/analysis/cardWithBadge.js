'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/analysis/cardWithBadge.html',
            restrict: 'E',
            scope: {
              symbol: '=',
              content: '='
            }
        };
    }
];
