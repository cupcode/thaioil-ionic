'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

  function() {
    return {
      templateUrl: 'templates/directiveViews/market/marketCard.html',
      restrict: 'E',
      scope: {
        data: '='
      }
    };
  }
];
