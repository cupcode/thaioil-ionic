'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:MainController
 * @description
 * # MainController
 */
module.exports = [
  '$scope', '$window', '$ionicFilterBar', '$state', '$ionicSlideBoxDelegate', 'allService','$rootScope','$location','$ionicHistory','$ionicPlatform','$ionicScrollDelegate','$ionicModal',

  function($scope, $window, $ionicFilterBar, $state, $ionicSlideBoxDelegate, allService,$rootScope,$location, $ionicHistory, $ionicPlatform,$ionicScrollDelegate, $ionicModal) {

    $ionicPlatform.ready(function() {

      console.log('viewHistory', $ionicHistory.viewHistory());

        $scope.$on("$ionicView.beforeEnter", function(event, data){
          if(ionic.Platform.isWebView()){
            screen.lockOrientation('portrait');
            console.log('lock portrait');
          }

        });

        $scope.lockSlide = function() {
          $ionicSlideBoxDelegate.enableSlide(false);
          $ionicSlideBoxDelegate.slide(2, 500);
        };

        $scope.Width = $window.innerWidth - 20;
        $scope.Height = $window.innerHeight - (44 + 40 + 47);

        $scope.listHeight = {
          'height': $scope.Height + 'px'
        };
        $scope.setWidth = {
          'width': $scope.Width + 'px'
        };

        $scope.slideHasChanged = function(index){
          $rootScope.$broadcast('slideHasChanged',{index: index, resource: 'economicCalendars'});
          // if(index == 2){
          //   getCountries();
          // }
        };

        var params = {
            'page': -1,
            'size': 999,
            'sort': 'publishDatetime,DESC',
            'projection':'mobileList'
        };

        var startThisWeek = moment().startOf('isoweek');
        var endThisWeek   = moment().endOf('isoweek');

        var startLastweek = startThisWeek.subtract(7, 'd');
        var endLastWeek   = endThisWeek.subtract(7, 'd');

        var startNextWeek = startThisWeek.add(7, 'd');
        var endNextWeek   = endThisWeek.add(7, 'd');

        $scope.lastweek = angular.copy(params);
        $scope.lastweek.startDate = moment().startOf('isoweek').subtract(7,'d').format('YYYY-MM-DD') + ' 00:00:00';
        $scope.lastweek.endDate   = moment().endOf('isoweek').subtract(7,'d').format('YYYY-MM-DD') + ' 23:59:59';
        $scope.lastweek.sort = 'publishDatetime,DESC';

        $scope.yesterday = angular.copy(params);
        $scope.yesterday.startDate = moment().subtract(1,'d').format('YYYY-MM-DD') + ' 00:00:00';
        $scope.yesterday.endDate = moment().subtract(1,'d').format('YYYY-MM-DD') + ' 23:59:59';
        $scope.yesterday.sort = 'publishDatetime,DESC';

        $scope.today = angular.copy(params);
        $scope.today.startDate = moment().format('YYYY-MM-DD') + ' 00:00:00';
        $scope.today.endDate = moment().format('YYYY-MM-DD') + ' 23:59:59';
        $scope.today.sort = 'publishDatetime,ASC';

        $scope.thisweek = angular.copy(params);
        $scope.thisweek.startDate = moment().startOf('isoweek').format('YYYY-MM-DD') + ' 00:00:00';
        $scope.thisweek.endDate   = moment().endOf('isoweek').format('YYYY-MM-DD') + ' 23:59:59';
        $scope.thisweek.sort = 'publishDatetime,ASC';

        $scope.nextweek = angular.copy(params);
        $scope.nextweek.startDate = moment().startOf('isoweek').add(7,'d').format('YYYY-MM-DD') + ' 00:00:00';
        $scope.nextweek.endDate   = moment().endOf('isoweek').add(7,'d').format('YYYY-MM-DD') + ' 23:59:59';
        $scope.nextweek.sort = 'publishDatetime,ASC';

        console.log('today:'     + $scope.today.startDate     + '-' + $scope.today.endDate);
        console.log('yesterday:' + $scope.yesterday.startDate + '-' + $scope.yesterday.endDate);
        console.log('lastweek:'  + $scope.lastweek.startDate  + '-' + $scope.lastweek.endDate);
        console.log('thisweek:'  + $scope.thisweek.startDate  + '-' + $scope.thisweek.endDate);
        console.log('nextweek:'  + $scope.nextweek.startDate  + '-' + $scope.nextweek.endDate);

        $scope.$on('goBack', function(){
            console.log('on goBack');
            $ionicHistory.goBack();
        });

        function getCountries(){
          $scope.countries = [];
          allService.allCalendarCountries()
          .then(function successCallback(response) {
              console.log('load countries', response);
              $scope.data = response.data._embedded.countries;
              var count = 0;
              $scope.data.forEach(function(country){
                  var tmp = {};
                  tmp.id = country.id;
                  tmp.checked = true;
                  tmp.name  = country.name;
                  $scope.countries.push(tmp);
              });
              $rootScope.countries = $scope.countries;
              $rootScope.$broadcast('setToggleAll',false); //refresh filter

          }, function errorCallback(response) {
              $scope.$broadcast('scroll.refreshComplete');
              console.log('Error allCalendarCountries',response);
          });
        }

        $rootScope.$on('refresh',function(event, location){
          //Run background -> resume
          if(location === 'all'){
            getCountries();
          }
        });

        $scope.$on("$ionicView.afterEnter", function(event, data){
          //Home -> economicCalendars
          getCountries();
        });
    });
  }
];
