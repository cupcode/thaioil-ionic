'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */

module.exports = ['templatesService',
  function (templatesService) {
    return {
      restrict: 'E',
      scope: {
        resource: '@',
        number: '=',
        searchMethod: '@',
        newsResource: '=',
        assignParams: '=',
        hasBreakingNews: '=',
        title: '=',
        searchId: '='
      },
      templateUrl: function ($elem, $attr) {
        return templatesService[$attr.type];
      },
      controller: ['$scope', '$timeout', '$window', 'allService', '$location', '$ionicPlatform', '$rootScope', '$translate', '$q', '$ionicScrollDelegate', '$ionicSlideBoxDelegate',
        function ($scope, $timeout, $window, allService, $location, $ionicPlatform, $rootScope, $translate, $q, $ionicScrollDelegate, $ionicSlideBoxDelegate) {

          $ionicPlatform.ready(function () {

            $scope.Height = $window.innerHeight - 136;
            $scope.baseUrl = $rootScope.baseUrl;
            $scope.loading = true;

            //FILTER NEWSSOURCES
            if ($scope.newsResource) {
              $scope.$on('newsSourceIds', function (event, args) {
                if (args) {
                  $scope.params['newsSourceIds'] = args;
                } else {
                  $scope.params['newsSourceIds'] = '9999'; //No newsSourceIds selected
                }
                $scope.loading = true;
                console.log('on newsSourceIds change refresh..', $scope.params);
                $scope.refresh();
              });
            }
            if ($scope.resource === 'economicCalendars') {

              $scope.$on('calendarCountries', function (event, args) {
                $scope.loading = true;
                $scope.params['countries'] = args;
                if (!args) {
                  $scope.params['countries'] = '9999'; //No countries selected
                }
                $scope.loading = true;
                console.log('on countries change refresh..', $scope.params);
                $scope.refresh();
              });
            }

            //SEARCH KEYWORD
            $scope.keyword = "";
            $scope.$on('search', function (event, args) {
              if ($location.path() === args.location) {
                $scope.keyword = args.keyword;
              }
            });
            $scope.$on('searchEnter', function (event, args) {
              if ($location.path() === args.location) {
                if (!args.keyword) {
                  $scope.params.keyword = '%%';
                  //Restore
                  if ($scope.storeItemsSearchEnter) {
                    $scope.items = $scope.storeItemsSearchEnter;
                    if ($scope.items.length == 0) {
                      $scope.noContent = true;
                    } else {
                      $scope.noContent = false;
                    }
                  }

                } else {
                  $scope.storeItemsSearchEnter = $scope.items;
                  $scope.params.keyword = args.keyword;
                  $scope.loading = true;
                  console.log('on searchEnter refresh..');
                  $scope.refresh();
                }
              }
            });

            $scope.breakingNewsToday = false;
            function loadBreakingNews() {

              allService.search('breakingNews', 'findAllAvailableNow', { page: 0, size: 1, projection: 'mobileList', sort: 'publishDatetime,DESC' })
                .then(function successCallback(response) {
                  $scope.breakingNews = response.data._embedded.breakingNews;
                  if ($scope.breakingNews.length > 0) {
                    $scope.breakingNews = $scope.breakingNews[0];
                    if (moment().format('YYYYMMDD') === moment($scope.breakingNews.publishDatetime).format('YYYYMMDD')) {
                      $scope.breakingNewsToday = true;
                      $rootScope.$broadcast('breakingNewsUpdate', $scope.breakingNews);
                    } else {
                      $scope.breakingNewsToday = false;
                      $rootScope.$broadcast('breakingNewsUpdate', null);
                    }
                  }

                });
            }
            $rootScope.$on('breakingNewsUpdate', function (event, breakingNews) {
              console.log('latest news receive breakingNewsUpdate');
              $scope.breakingNews = breakingNews;
              if ($scope.breakingNews) {
                $scope.breakingNewsToday = true;
              }
              else {
                $scope.breakingNewsToday = false;
              }
            });

            $scope.$on('online', function (event, args) {
              $scope.loading = true;
              $scope.refresh();
            });
            $rootScope.$on('refresh', function (event, location) {
              console.log('refresh slidePage: ', $location.path(), location);
              if ($location.path() === location) {
                //tap on tab menu -> scrollTop and refrsh if has notification
                if (($scope.resource === 'analyses' && $rootScope.analysisCount > 0) ||
                  ($scope.resource === 'weeklyAnalyses' && $rootScope.weeklyAnalysisCount > 0) ||
                  ($scope.resource === 'specialAnalyses' && $rootScope.specialAnalysisCount > 0) ||
                  ($scope.resource === 'news' && $scope.hasBreakingNews && $rootScope.newsCount > 0) //refresh only latest news
                ) {
                  $scope.loading = true;
                  $scope.refresh();
                }
              }
              else if (location === 'all') {
                delete $scope.params['countries'];
                $scope.loading = true;
                $scope.refresh();
              }
            });

            //NOTIFICATION HANDLER
            function updateBadge() {
              if ($scope.resource === 'news' && $scope.searchMethod === 'findByTitle') {
                $rootScope.newsCount = 0;
                $rootScope.$broadcast('newsCount');
                $rootScope.breakingNewsCount = 0;
                $rootScope.$broadcast('breakingNewsCount');
              }
              else if ($scope.resource === 'analyses') {
                $rootScope.analysisCount = 0;
                $rootScope.$broadcast('analysisCount');
              }
              else if ($scope.resource === 'weeklyAnalyses') {
                $rootScope.weeklyAnalysisCount = 0;
                $rootScope.$broadcast('weeklyAnalysisCount');
              }
              else if ($scope.resource === 'specialAnalyses') {
                $rootScope.specialAnalysisCount = 0;
                $rootScope.$broadcast('specialAnalysisCount');
              }
            }
            function showBubble(badgeCount) {
              if (badgeCount > 0) {
                $scope.showBubble = true;
              }
              else {
                $scope.showBubble = false;
              }
            }
            if ($scope.resource === 'analyses') {
              showBubble($rootScope.analysisCount);
              $rootScope.$on('analysisCount', function (events, count) {
                showBubble($rootScope.analysisCount);
                console.log('showBubble analysis ' + $rootScope.analysisCount);
              });
            }
            else if ($scope.resource === 'weeklyAnalyses') {
              showBubble($rootScope.weeklyAnalysisCount);
              $rootScope.$on('weeklyAnalysisCount', function (events, count) {
                showBubble($rootScope.weeklyAnalysisCount);
                console.log('showBubble weekly ' + $rootScope.weeklyAnalysisCounts);
              });
            }
            else if ($scope.resource === 'news' && $scope.hasBreakingNews) {
              showBubble($rootScope.newsCount);
              $rootScope.$on('newsCount', function (events, count) {
                showBubble($rootScope.newsCount);
                console.log('showBubble news ' + $rootScope.newsCount);
              });
            }
            else if ($scope.resource === 'specialAnalyses') {
              showBubble($rootScope.specialAnalysisCount);
              $rootScope.$on('specialAnalysisCount', function (events, count) {
                showBubble($rootScope.specialAnalysisCount);
                console.log('showBubble special ' + $rootScope.specialAnalysisCount);
              });
            }

            $scope.scrollTopAndRefresh = function () {
              $ionicScrollDelegate.scrollTop('animate');
              $timeout(function () {
                $scope.refresh();
              }, 300);
            }

            $scope.refresh = function () {
              $scope.params.page = 0;
              $scope.noContent = false;

              if ($scope.hasBreakingNews) {
                loadBreakingNews();
              }

              allService.search($scope.resource, $scope.searchMethod, $scope.params)
                .then(function successCallback(response) {
                  console.log('refresh', $scope.resource, $scope.searchMethod, $scope.params, response);

                  var data = response.data;

                  if (!data['_embedded'][$scope.resource]) {
                    $scope.items = [];
                    if ($scope.resource == 'promotionCategories') {
                      $scope.items = data['_embedded']['promotions'];
                    }

                  } else if (data.page && data.page.number < data.page.totalPages) {
                    if ($scope.resource == 'promotionCategories') {
                      $scope.items = data['_embedded']['promotions'];
                    } else {
                      $scope.items = data['_embedded'][$scope.resource];
                      if ($scope.resource === 'economicCalendars') {
                        // getCountries()
                      }
                    }
                    $scope.canLoad = true;

                  } else {
                    $scope.canLoad = false;
                  }

                  if (_.isUndefined($scope.items) || $scope.items.length == 0) {
                    $scope.noContent = true;
                  }
                  console.log('before $scope.items ', $scope.items)
                  if ($scope.resource === 'analyses' || $scope.resource === 'weeklyAnalyses' || $scope.resource === 'interviews' || $scope.resource === 'specialAnalyses') {
                    $scope.items = $scope.resizeImage($scope.items)
                  } else if ($scope.resource === 'news') {
                    $scope.items = $scope.redirectImage($scope.items)
                  }
                  console.log('after $scope.items ', $scope.items)

                  $ionicSlideBoxDelegate.update()
                  $scope.$broadcast('scroll.refreshComplete');
                  $scope.loading = false; //after filter newsSourceIds & searchEnter & refresh noti & back to online
                  updateBadge();

                }, function errorCallback(response) {
                  $scope.$broadcast('scroll.refreshComplete');
                  console.log('Error list resource=' + $scope.resource, response);
                });

            };

            $scope.loadMore = function () {

              if ($scope.hasBreakingNews) {
                loadBreakingNews();
              }

              $scope.params.page = $scope.params.page + 1;
              allService.search($scope.resource, $scope.searchMethod, $scope.params)
                .then(function successCallback(response) {
                  console.log('loadmore resource: ' + $scope.resource + '' + $scope.searchMethod + ' ' + $scope.number, response);

                  var data = response.data;
                  if (data.page && data.page.number < data.page.totalPages) {
                    if ($scope.resource === 'news') {
                      $scope.items = $scope.items.concat($scope.redirectImage(data['_embedded'][$scope.resource]));
                    } else {
                      $scope.items = $scope.items.concat($scope.resizeImage(data['_embedded'][$scope.resource]));
                    }
                  } else {
                    $scope.canLoad = false;
                  }
                  $scope.$broadcast('scroll.infiniteScrollComplete');

                }, function errorCallback(response) {
                  $scope.canLoad = false;
                  console.log('Error list resource=' + $scope.resource, response);
                });

            };

            //INIT PAGE
            $scope.items = [];

            console.log('$scope.searchId', $scope.searchId)
            console.log('$scope.assignParams', $scope.assignParams)
            if (!$scope.assignParams) {
              //default params
              $scope.params = {
                'page': -1,
                'size': 10,
                'sort': 'publishDatetime,DESC',
                'projection': 'mobileList',
                'keyword': '%%',
                'locale': 'TH'
              }
              if ($scope.searchId) {
                $scope.params.id = $scope.searchId
                $scope.params.sort = ''
              }
            } else {
              $scope.params = $scope.assignParams;
              if ($scope.searchId) {
                $scope.params.id = $scope.searchId
              }
            }

            $scope.first = true;
            $scope.noContent = false;
            $scope.canLoad = false;
            if ($scope.resource === 'economicCalendars' || $scope.resource === 'promotionCategories') {
              //Init: load all slides
              $scope.loading = true;
              $scope.noContent = false;
              $timeout(function () {
                $scope.refresh();
                $scope.first = false;
              }, 1000);

            } else {
              //Init: load only first slide
              if ($scope.number == 0) {
                $scope.loading = true;
                $scope.noContent = false;
                $timeout(function () {
                  $scope.refresh();
                  $scope.first = false;
                }, 1000);
              }
            }

            $scope.$on('slideHasChanged', function (event, args) {
              //!economicCalendars slide load
              console.log('slideHasChanged', args)
              if ($scope.first && $scope.number === args.index && (args.resource === $scope.resource ||
                (args.resource === 'analyses' &&
                  ($scope.resource === 'weeklyAnalyses' || $scope.resource === 'analyses' ||
                    $scope.resource === 'specialAnalyses' || $scope.resource === 'interviews')
                ))
              ) {
                $scope.loading = true;
                $scope.noContent = false;
                $timeout(function () {
                  $scope.refresh();
                  $scope.first = false;
                }, 1000);
              } else if (args.resource === 'promotionCategories') {
                $scope.loading = true;
                $scope.noContent = false;
                $timeout(function () {
                  $scope.refresh();
                  $scope.first = false;
                }, 1000);
              }

              //economicCalendars reload all slides when select TODAY
              // if(!$scope.first && args.index === 2 && 'economicCalendars' === args.resource){ //
              //   $scope.loading = true;
              //   $scope.noContent = false;
              //   $timeout(function(){
              //     $scope.refresh();
              //   },1000);
              // }
            });

            function getCountries() {
              allService.allCalendarCountries()
                .then(function successCallback(response) {
                  $scope.data_countries = response.data[_embedded][countries];
                  console.log('getCountries resp', $scope.data_countries)
                  $scope.items.forEach(function (economicCalendar) {
                    economicCalendar.country = _.find($scope.data_countries, { 'id': economicCalendar.countryId });
                    console.log('economicCalendar.country', economicCalendar.country)
                  })

                }, function errorCallback(response) {
                  $scope.$broadcast('scroll.refreshComplete');
                  console.log('Error allCalendarCountries', response);
                });
            }

            $scope.resizeImage = function (data) {
              for (var i = 0; i < data.length; i++) {
                if (data && data[i].thumbnailPath !== null) {
                  var dataSplit = data[i].thumbnailPath.split('/')
                  var result = ''
                  for (var j = 0; j < dataSplit.length; j++) {
                    if (j === dataSplit.length - 1) {
                      result = result + '/resize_500_' + dataSplit[j]
                    } else if (j === 0) {
                      result = dataSplit[j]
                    } else {
                      result = result + '/' + dataSplit[j]
                    }
                  }
                  data[i].thumbnailPath = result
                }
              }
              return data
            }


            $scope.redirectImage = function (data) {

              console.log('redirectImage', data)
              for (var i = 0; i < data.length; i++) {
                if (data && data[i].newsSourceName === 'TNN24') {
                  var dataSplit = data[i].ogImage.split('/')
                  var result = ''
                  for (var j = 0; j < dataSplit.length; j++) {
                    if (j === dataSplit.length - 1) {
                      result = result + '/' + dataSplit[j]
                    } else if (j === 0) {
                      result = dataSplit[j]
                    } else {
                      result = result + '/' + dataSplit[j]
                    }
                  }
                  data[i].ogImage = result
                }
              }
              return data

              // if (data.newsSourceName === 'TNN24') {
              //   var dataSplit = data.ogImage.split("/")
              //   var result = ''
              //   for (var i = 0; i < dataSplit.length; i++) {
              //     if (i === dataSplit.length - 1) {
              //       result = result + '/thumb_big/' + dataSplit[i]
              //     } else if (i === 0) {
              //       result = dataSplit[i]
              //     } else {
              //       result = result + '/' + dataSplit[i]
              //     }
              //   }

              //   console.log('redirectImage', data, result)
              //   return result
              // } else {
              //   return 'images/img/no_image300x300.png'
              // }
            }

          });
        }]//controller
    }; //return
  }]; //module
