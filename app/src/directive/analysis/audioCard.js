'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/analysis/audioCard.html',
            restrict: 'E',
            scope: {
              data: '=',
              theme: '='
            },
            link: function(scope, elem, attr){
              console.log('audioCard:',scope.data);
              console.log('audioCard:',scope.theme);
            }
        };
    }
];
