'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope', '$rootScope', '$state', '$translate', '$cordovaNativeAudio', '$cordovaMedia', '$ionicPlatform', '$ionicModal', 'allService', '$ionicPopup', '$ionicHistory', '$cordovaPushV5', '$location', '$ionicScrollDelegate', '$timeout',

  function ($scope, $rootScope, $state, $translate, $cordovaNativeAudio, $cordovaMedia, $ionicPlatform, $ionicModal, allService, $ionicPopup, $ionicHistory, $cordovaPushV5, $location, $ionicScrollDelegate, $timeout) {

    $ionicPlatform.ready(function () {

      $rootScope.audioSrc = null; $scope.audioSrc = $rootScope.audioSrc;
      $rootScope.audioPlaying = false;
      $scope.playing = false;

      function initPlayer(item) {

        $scope.audioTitle = item.audioTitle;

        $rootScope.audioSrc = item.audioUrl; $scope.audioSrc = $rootScope.audioSrc; //New Source
        $rootScope.audioPlaying = true; $scope.playing = $rootScope.audioPlaying; //Status

        if (item.audioUrl.indexOf("http") === -1) {
          if (ionic.Platform.isAndroid()) {
            $scope.media = new Audio();
            $scope.media.src = $rootScope.baseUrl + $rootScope.audioSrc;
            $scope.media.load();
          } else {
            $scope.media = $cordovaMedia.newMedia($rootScope.baseUrl + $rootScope.audioSrc);
          }
        } else {

          var url = $rootScope.audioSrc.substring(0, $rootScope.audioSrc.lastIndexOf("/") + 1);
          var fileName = $rootScope.audioSrc.substring($rootScope.audioSrc.lastIndexOf("/") + 1);
          if (ionic.Platform.isAndroid()) {
            $scope.media = new Audio();
            $scope.media.src = url + fileName;
            $scope.media.load();
          } else {
            $scope.media = $cordovaMedia.newMedia(url + encodeURIComponent(fileName));
          }

        }

        $scope.media.play();

        function createMusicControlsAndroid() {
          MusicControls.create({
            track: $scope.audioTitle,		// optional, default : ''
            artist: $scope.audioTitle,						// optional, default : ''
            cover: 'images/icon.png',		// optional, default : nothing
            isPlaying: true,							// optional, default : true
            dismissable: false,							// optional, default : false

            // hide previous/next/close buttons:
            hasPrev: false,		// show previous button, optional, default: true
            hasNext: false,		// show next button, optional, default: true
            hasClose: true,		// show close button, optional, default: false

            // Android only, optional
            // text displayed in the status bar when the notification (and the ticker) are updated
            ticker: 'Now playing "' + $scope.audioTitle + '"'
          }, onSuccess, onError);
        }

        if (ionic.Platform.isAndroid()) {
          createMusicControlsAndroid();
          document.addEventListener("pause", createMusicControlsAndroid, false);
        }
        else if (ionic.Platform.isIOS()) {

          var artist = $scope.audioTitle;
          var title = $scope.audioTitle;
          var album = null;
          var image = 'images/icon.png';
          var duration = $scope.media.getDuration();
          var elapsedTime = null;//$scope.media.getElapsedTime();

          var params = [artist, title, album, image, duration, elapsedTime];
          window.remoteControls.updateMetas(function (success) {
            console.log("succes:", success);
          }, function (fail) {
            console.log("fail:", fail);
          }, params);

          console.log("window.remoteControls.updateMetas");

          //listen for the event
          document.addEventListener("remote-event", function (event) {

            switch (event.remoteEvent.subtype) {
              case "play":
                console.log('play');
                $scope.$apply(function () {
                  $scope.play();
                });
                break;
              case "pause":
                console.log('pause');
                $scope.$apply(function () {
                  $scope.pause();
                });
                break;
              case "playpause":
                console.log("playpause");
                break;
              case "skip":
                console.log('skip');
                break;
              case "back":
                console.log('back');
                break;
            }

          });
        }

      }

      function onSuccess(e) {
        console.log('onSuccess:', e);
      }
      function onError(e) {
        console.log('onError:', e);
      }
      function events(action) {
        switch (action) {
          case 'music-controls-next':
            break;
          case 'music-controls-previous':
            break;
          case 'music-controls-pause':
            $scope.$apply(function () {
              $scope.pause();
            });
            break;
          case 'music-controls-play':
            $scope.$apply(function () {
              $scope.play();
            });
            break;
          case 'music-controls-destroy':
            console.log('music-controls-destroy');
            $scope.$apply(function () {
              $scope.stop();
            });
            break;
          // Headset events (Android only)
          case 'music-controls-media-button':
            console.log('music-controls-media-button');
            break;
          case 'music-controls-headset-unplugged':
            console.log('music-controls-headset-unplugged');
            break;
          case 'music-controls-headset-plugged':
            console.log('music-controls-headset-plugged');
            break;
          default:
            break;
        }
      }

      if (ionic.Platform.isAndroid()) {
        MusicControls.subscribe(events);
        MusicControls.listen();
      }

      $scope.$on('audioPlaying', function (event, item) {
        if (item.audioUrl != $rootScope.audioSrc) {
          console.log('Init Player');
          if ($scope.media) {
            $scope.stop();
          }
          initPlayer(item);
        } else {
          $scope.play();
        }
      });
      $scope.play = function () {
        $scope.media.play();
        if (ionic.Platform.isAndroid()) { MusicControls.updateIsPlaying(true); }
        $rootScope.audioPlaying = true; $scope.playing = $rootScope.audioPlaying;//Status
        $rootScope.$broadcast('mainAudioPlaying');
      };
      $scope.$on('audioPausing', function (event) {
        $scope.pause();
      });
      $scope.pause = function () {
        $scope.media.pause();
        if (ionic.Platform.isAndroid()) { MusicControls.updateIsPlaying(false); }
        $rootScope.audioPlaying = false; $scope.playing = $rootScope.audioPlaying; //Status
        $rootScope.$broadcast('mainAudioPausing');
      };
      $scope.stop = function () {

        $rootScope.$broadcast('mainAudioEnded');

        if (ionic.Platform.isAndroid()) {
          $scope.media.pause();
          $scope.media.src = null;
          MusicControls.destroy(onSuccess, onError);
        } else {
          $scope.media.stop();
        }
        $rootScope.audioSrc = null; $scope.audioSrc = $rootScope.audioSrc; //Remove Source
        $rootScope.audioPlaying = false; $scope.playing = false; //Status

      };

      //NOTIFICATION CENTER
      $ionicModal.fromTemplateUrl('templates/views/analysisDetailModal.html', {
        id: 2,
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.analysisModal = modal;
      });

      $ionicModal.fromTemplateUrl('templates/views/newsDetailModal.html', {
        id: 3,
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.newsModal = modal;
      });
      $scope.closeNewsModal = function () {
        $scope.newsModal.hide();
      };
      $scope.closeAnalysisModal = function () {
        $scope.analysisModal.hide();
      };

      function setAppBadge() {
        if (ionic.Platform.isIOS()) {
          var total = $rootScope.newsCount + $rootScope.breakingNewsCount + $rootScope.calendarCount + $rootScope.analysisCount + $rootScope.weeklyAnalysisCount + $rootScope.specialAnalysisCount;
          $cordovaPushV5.setBadgeNumber(total).then(function (result) {
            console.log('iOs setBadgeNumber then', result);
          }, function (err) {
            // handle error
          });
        }
      }

      $scope.newsCount = $rootScope.newsCount;
      $rootScope.$on('newsCount', function (events) {
        $scope.newsCount = $rootScope.newsCount;
        console.log('newsCount')
        setAppBadge();
      });
      $scope.breakingNewsCount = $rootScope.breakingNewsCount;
      $rootScope.$on('breakingNewsCount', function (events) {
        $scope.breakingNewsCount = $rootScope.breakingNewsCount;
        setAppBadge();
      });
      $scope.calendarCount = $rootScope.calendarCount;
      $rootScope.$on('calendarCount', function (events, count) {
        $scope.calendarCount = $rootScope.calendarCount;
        setAppBadge();
      });
      $scope.analysisCount = $rootScope.analysisCount;
      $rootScope.$on('analysisCount', function (events, count) {
        $scope.analysisCount = $rootScope.analysisCount;
        setAppBadge();
      });
      $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
      $rootScope.$on('weeklyAnalysisCount', function (events) {
        $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
        setAppBadge();
      });
      $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
      $rootScope.$on('specialAnalysisCount', function (events) {
        $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
        setAppBadge();
      });
      $scope.selectTab = function (tab, path) {

        $rootScope.devMode = false;

        if ($location.path().indexOf(path) !== -1) { //already open this tab -> scrollTop
          $ionicScrollDelegate.scrollTop('animate');
          $timeout(function () {
            $state.go(tab);
            $rootScope.$broadcast('refresh', path); //tab/home, tab/analyses, tab/news
          }, 300);

        } else {
          $state.go(tab);
        }
      };

    });

  }

];
