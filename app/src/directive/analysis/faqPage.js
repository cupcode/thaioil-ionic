'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */


'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:faqPage
 * @description
 * # faqPage
 */
module.exports = [

  function () {
    return {
      templateUrl: 'templates/directiveViews/analysis/faqPage.html',
      restrict: 'E',
      scope: {
        resource: '@',
        searchMethod: '@'
      },
      controller: ['$scope', 'allService', '$window', '$sce', '$rootScope', function ($scope, allService, $window, $sce, $rootScope) {

        $scope.Height = $window.innerHeight - 140;
        $scope.params = {};

        $rootScope.$on('fontSizeChanged', function (event, args) {
          console.log('fontSizeChanged...');
          $scope.fontSize = parseInt(localStorage.fontSize);
        });

        if (localStorage.fontSize) {
          $scope.fontSize = parseInt(localStorage.fontSize);
        } else {
          $scope.fontSize = $rootScope.fonts[0];
        }
        $scope.toggleFont = function () {
          var index = _.indexOf($rootScope.fonts, $scope.fontSize);
          if (index === $rootScope.fonts.length - 1) {
            index = -1;
          }
          $scope.fontSize = $rootScope.fonts[index + 1];
          localStorage.fontSize = $scope.fontSize;
        }

        $scope.refresh = function () {
          $scope.item = null;
          allService.get($scope.resource, $scope.searchMethod, $scope.params)
            .then(function successCallback(response) {

              $scope.items = response.data._embedded.faqContents;
              console.log('faqContents: ', $scope.items)
              $scope.$broadcast('scroll.refreshComplete');

            }, function errorCallback(response) {
              console.log('Error list resource=' + $scope.resource, response);
              if (response.status === 404) {
                allService.search('faqs', 'findActive', $scope.params)
                  .then(function successCallback(response) {
                    console.log('faqs', response)
                    $scope.items = response.data.content;
                    console.log('faqContents: ', $scope.items)
                    $scope.$broadcast('scroll.refreshComplete');

                  }, function errorCallback(response) {
                    console.log('Error list resource=faq', response);
                  })
              } else {
                $scope.$broadcast('scroll.refreshComplete');
              }
            });
        };
        $scope.refresh();

        $scope.trust = function (html_code) {
          return $sce.trustAsHtml(html_code);
        };

      }]
    };
  }
];
