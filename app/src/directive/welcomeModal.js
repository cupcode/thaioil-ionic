'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [
    function()
    {
        return {
            restrict: 'E',
            controller: ['$scope','$rootScope','$ionicModal','$ionicPlatform','$ionicLoading','$ionicSlideBoxDelegate','$timeout', 'allService', 
            function($scope,$rootScope,$ionicModal,$ionicPlatform,$ionicLoading,$ionicSlideBoxDelegate, $timeout, allService){
            $ionicPlatform.ready(function() {

                $scope.serverUrl = JSON.parse(localStorage.getItem('server')).url

                $scope.options = {
                    loop: false,
                    initialSlide: 0,
                    direction: 'horizontal', //or vertical
                    speed: 300, //0.3s transition
                    autoplay: 3000
                }

                $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
                    // note: the indexes are 0-based
                    // $scope.activeIndex = data.slider.activeIndex;
                    // $scope.previousIndex = data.slider.previousIndex;
                    // if (data.slider.activeIndex === $scope.slides.length - 1) {
                    //     $ionicSlideBoxDelegate.stop();
                    //     $scope.timer = $timeout(function() {
                    //         // $scope.closeModal()
                    //     }, 2500)
                    // }
                });
                $scope.closeModal = function(){
                    // $timeout.cancel($scope.timer);
                    $scope.modal.hide();
                    $rootScope.welcome = true;
                    $rootScope.$broadcast("welcomeModalClose");
                };
                
                function imageExists(url, callback) {
                    var img = new Image();
                    img.onload = function() { callback(true); };
                    img.onerror = function() { callback(false); };
                    img.src = url;
                }

                if(!$rootScope.welcome){

                    $scope.slides
                    allService.getWelcomeSlides()
                    .then(function successCallback(result){

                        if (result.data.page.totalElements > 0) {
                            $scope.slides = result.data._embedded.welcomeSlides
                            // check valid image
                            $ionicLoading.show();
                            imageExists($scope.serverUrl + $scope.slides[0].image, function(exists) {
                                $ionicLoading.hide();
                                if(exists){
                                    $ionicModal.fromTemplateUrl('templates/views/welcomeModal.html', {
                                    scope: $scope
                                    }).then(function(modal) {
                                        $scope.modal = modal;
                                        $scope.modal.show();
                                    });
                                    $scope.closeModal = function(){
                                        $scope.modal.hide();
                                        $rootScope.welcome = true;
                                        $rootScope.$broadcast("welcomeModalClose");
                                    };
                                }else{
                                    console.log("Welcome Image doesn't exist");
                                    $rootScope.$broadcast("welcomeModalClose");
                                    $rootScope.welcome = true;
                                }
                            });
                        }
                        else {
                            $rootScope.$broadcast("welcomeModalClose");
                            $rootScope.welcome = true;
                        }

                    },function errorCallback(response){
                        console.log('cannot get lastWeekCalendar', response);
                    });
                    
                }

              })

            }]
        };
    }
];
