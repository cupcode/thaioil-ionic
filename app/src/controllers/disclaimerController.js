'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:MainController
 * @description
 * # MainController
 */
module.exports = [
  '$scope','$ionicHistory','$rootScope',

  function($scope,$ionicHistory,$rootScope) {

    $scope.$on("$ionicView.beforeEnter", function(event, data){
      if(ionic.Platform.isWebView()){
        screen.lockOrientation('portrait');
        console.log('lock portrait');
      }
    });

    if(localStorage.fontSize){
      $scope.fontSize = parseInt(localStorage.fontSize);
    }else{
      $scope.fontSize = $rootScope.fonts[0];
    }

    $scope.toggleFont = function(){
      var index = _.indexOf($rootScope.fonts, $scope.fontSize);
      if(index === $rootScope.fonts.length-1 ){
        index = -1;
      }
      $scope.fontSize = $rootScope.fonts[index+1];
      localStorage.fontSize = $scope.fontSize;
      $rootScope.$broadcast('fontSizeChanged');
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

  }
];
