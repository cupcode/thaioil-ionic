'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */
module.exports = [
  function () {
    return {
      restrict: 'E',
      scope: {
        title: '@',
        'e95': '=',
        'e91': '=',
        'e20': '=',
        'diesel': '=',
        'tax': '@',
      },
      templateUrl: 'templates/directiveViews/home/oilDirectionCard.html',
      controller: ['$scope', 'allService', '$ionicPlatform', '$filter', '$q', '$rootScope', function ($scope, allService, $ionicPlatform, $filter, $q, $rootScope) {

        $ionicPlatform.ready(function () {
          console.log('$scope.e95', $scope)
          $scope.resource = 'retailPrices';
          $scope.searchMethod = 'findLastestRetailPrice';

          $scope.refresh = function () {
            $q.all([
              allService.getById("retailTaxes", 1),
              allService.search($scope.resource, $scope.searchMethod),
            ]).then(function successCallback(response) {
              console.log('response oilDirectionCart', response)
              var tax = parseFloat(response[0].data.tax);
              var data = response[1].data;
              $scope.temp = data._embedded[$scope.resource];

              if ($scope.temp) {
                for (var i = 0; i < $scope.temp.length; i++) {
                  if ($scope.temp[i].nameEn === 'PTT') {
                    $scope.ptt = $scope.temp[i];
                  }
                }
                if (moment().format("MM-DD-YYYY") === moment($scope.ptt.updatedAt).format("MM-DD-YYYY")) {
                  $scope.showDifferentPrice = true
                }

                $scope.ptt.differentGasohol95 = parseFloat($scope.ptt.differentGasohol95).toFixed(2)
                $scope.ptt.differentGasohol91 = parseFloat($scope.ptt.differentGasohol91).toFixed(2)
                $scope.ptt.differentGasoholE20 = parseFloat($scope.ptt.differentGasoholE20).toFixed(2)
                $scope.ptt.differentDiesel = parseFloat($scope.ptt.differentDiesel).toFixed(2)

                $scope.ptt.gasohol91 = parseFloat($scope.ptt.gasohol91) + tax
                $scope.ptt.gasohol95 = parseFloat($scope.ptt.gasohol95) + tax
                $scope.ptt.gasoholE20 = parseFloat($scope.ptt.gasoholE20) + tax
                $scope.ptt.gasoholE85 = parseFloat($scope.ptt.gasoholE85) + tax
                $scope.ptt.gasoline95 = parseFloat($scope.ptt.gasoline95) + tax
                $scope.ptt.diesel = parseFloat($scope.ptt.diesel) + tax

                console.log('$scope.ptt:', $scope.ptt);
              }

            }, function errorCallback(response) {
              console.log('Error search resource:' + $scope.resource + ',searchMethod:' + $scope.searchMethod, response);
            });

          }

          $scope.refresh()
          $rootScope.$on('taxRefresh', function (events, count) {
            $scope.refresh()
          });

        });

      }]
    };
  }
];
