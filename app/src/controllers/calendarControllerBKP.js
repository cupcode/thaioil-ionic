'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */

module.exports = ['$scope','$state','allService','$location','$rootScope','$ionicHistory','$ionicPlatform','$ionicScrollDelegate',

function ($scope,$state,allService,$location,$rootScope,$ionicHistory,$ionicPlatform, $ionicScrollDelegate)
{
  $ionicPlatform.ready(function() {

    // $scope.keyword = "";
    // $scope.$on('search',function(event,args){
    //   if($location.path() === args.location){
    //     $scope.keyword = args.keyword;
    //   }
    // });

    // $scope.resource = 'economicCalendars';
    // $scope.searchMethod = 'findBetween';
    // $scope.items = [];
    // $scope.params = {
    //   'page': -1,
    //   'size': 10,
    //   'sort': 'publishDatetime,DESC',
    //   'projection':'mobileList',
    //   'startDate': moment().subtract(1,'d').format('YYYY-MM-DD') + ' 00:00:00', //-1day
    //   'endDate':moment().add(14,'d').format('YYYY-MM-DD') + ' 00:00:00', //+2week
    // };

    $scope.refresh = function() {
      $scope.items = [];
      $scope.params.page = 0;
      $scope.noContent = false;
      allService.search($scope.resource, $scope.searchMethod, $scope.params)
      .then(function successCallback(response) {
        console.log('refresh list: ',response);
        var data = response.data;
        if(data.page && data.page.number < data.page.totalPages){
          $scope.items = data['_embedded'][$scope.resource];
          $scope.canLoad = true;
        }
        else{
          $scope.canLoad = false;
        }

        if($scope.items.length == 0){
          $scope.noContent = true;
        }
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loading = false; //after filter newsSourceIds & searchEnter

        $rootScope.calendarCount = 0;
        $rootScope.$broadcast('calendarCount');

      }, function errorCallback(response) {
          $scope.$broadcast('scroll.refreshComplete');
          console.log('Error list resource=' + $scope.resource,response);
      });

    };

    $scope.loadMore = function() {

      $scope.params.page = $scope.params.page + 1;
      allService.search($scope.resource, $scope.searchMethod, $scope.params)
      .then(function successCallback(response) {

        var data = response.data;
        if(data.page && data.page.number < data.page.totalPages){
          $scope.items = $scope.items.concat(data['_embedded'][$scope.resource]);
        }else{
          $scope.canLoad = false;
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }, function errorCallback(response) {
          $scope.canLoad = false;
          console.log('Error list resource=' + $scope.resource,response);
      });

    };

    $scope.canLoad = true;
    $scope.loadMore();

    $scope.$on('goBack', function(){
      $ionicHistory.goBack();
    });
    

  });
}];
