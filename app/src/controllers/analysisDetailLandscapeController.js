'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:MainController
 * @description
 * # MainController
 */
module.exports = [
  '$scope','$ionicHistory','$stateParams','$location','$rootScope','$timeout','$ionicScrollDelegate',

  function($scope,$ionicHistory,$stateParams,$location,$rootScope,$timeout,$ionicScrollDelegate) {

    $scope.item = $stateParams.item;
    $scope.resource = $stateParams.resource;
    
    console.log("$scope.item : ", $scope.item)
    if(localStorage.fontSize){
      $scope.fontSize = parseInt(localStorage.fontSize);
    }else{
      $scope.fontSize = $rootScope.minFontSize;
    }

    $scope.$on('orientationchange', function(event, args) {
      console.log('orientationchange:', args);
      if ((args === 'portrait-primary' || args === 'portrait-secondary') && $location.path().indexOf('analysisDetailLandscape') > -1) {
        $ionicHistory.goBack();
      }
    });

    $scope.selectedTab = 1;
    $scope.selectTab = function(tabIndex){
      $scope.selectedTab = tabIndex;
      $timeout(function () {
        $ionicScrollDelegate.resize();
        console.log('resize..')
      }, 100);
    };

  }
];
