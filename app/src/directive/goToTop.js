

    'use strict';

    /**
     * @ngdoc function
     * @name Thaioil.directive:marketCard
     * @description
     * # marketCard
     */
    module.exports = ['$ionicScrollDelegate','$location','$rootScope','$timeout',

        function($ionicScrollDelegate,$location,$rootScope,$timeout)
        {
          return {
              restrict: 'A',
              link: function(scope, element, attrs) {
                  element.bind('click', function() {
                    $ionicScrollDelegate.scrollTop('animate');
                    $timeout(function(){
                        $rootScope.$broadcast('refresh',$location.path());
                    },300);

                  });
              }
          };
        }
    ];
