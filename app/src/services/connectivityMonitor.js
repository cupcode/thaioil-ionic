'use strict';

/**
 * @ngdoc function
 * @name Thaioil.service:ExampleService
 * @description
 * # ExampleService
 */
module.exports = ['$rootScope', '$cordovaNetwork','$ionicPopup','$cordovaDialogs','$translate','$timeout',
  function($rootScope, $cordovaNetwork, $ionicPopup, $cordovaDialogs,$translate,$timeout) {

    return {
        isOnline: function(){
          if(ionic.Platform.isWebView()){
            return $cordovaNetwork.isOnline();
          } else {
            return navigator.onLine;
          }
        },
        isOffline: function(){
          if(ionic.Platform.isWebView()){
            return !$cordovaNetwork.isOnline();
          } else {
            return !navigator.onLine;
          }
        },
        startWatching: function(){

            if(ionic.Platform.isWebView()){

              if($cordovaNetwork.isOnline()){
                $rootScope.$broadcast('online');
                console.log('$cordovaNetwork.isOnline()');
              }else{
                $rootScope.$broadcast('offline');
                console.log('$cordovaNetwork.isOffline()');
              }

              $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                $rootScope.$broadcast('online');
                console.log('$cordovaNetwork:online');
              });

              $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

                $timeout(function(){
                  if($cordovaNetwork.isOnline()){
                    console.log('$cordovaNetwork:offline but just switch to another network ' + networkState);
                  }else{
                    console.log('$cordovaNetwork:offline');
                    $rootScope.$broadcast('offline');

                    $cordovaDialogs.alert(
                      $translate.instant('NO_INTERNET'),
                      $translate.instant('ERROR'),
                      $translate.instant('CONFIRM'))
                    .then(function() {

                    });
                  }
                }, 1000);

              });

            }
            else {

              if(navigator.onLine){
                $rootScope.$broadcast('online');
                console.log('navigator.onLine')
              }else{
                $rootScope.$broadcast('offline');
                console.log('navigator.offLine')
              }

              window.addEventListener("online", function(e) {
                $rootScope.$broadcast('online');
                console.log("addEventListener online");
              }, false);

              window.addEventListener("offline", function(e) {
                $rootScope.$broadcast('offline');
                console.log("addEventListener offline");
                $cordovaDialogs.alert(
                  $translate.instant('NO_INTERNET'),
                  $translate.instant('ERROR'),
                  $translate.instant('CONFIRM'))
                .then(function() {

                });
              }, false);
            }
        }
      };

  }
];
