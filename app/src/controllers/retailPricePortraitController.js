'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:SettingsController
 * @description
 * # SettingsController
 */
module.exports = [
  '$scope', '$ionicHistory', '$translate', '$rootScope', '$stateParams', '$state',

  function ($scope, $ionicHistory, $translate, $rootScope, $stateParams, $state) {
    {
      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (ionic.Platform.isWebView()) {
          screen.lockOrientation('portrait');
          console.log('lock portrait');
        }
      });

      $scope.serverUrl = JSON.parse(localStorage.getItem('server')).url + '/gasStations/logo/'
      console.log('$stateParams', $stateParams);
      $scope.items = $stateParams.items;
      $scope.tax = $stateParams.tax;
      $scope.selectedIndex = $stateParams.index;
      $scope.options = {
        loop: true,
        pagination: false,
        initialSlide: $scope.selectedIndex
      }
      $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
        $scope.slider = data.slider;
      });

      $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
        if ($scope.items[data.activeIndex].nameEn === 'Chevron') {
          $scope.currentGasStation = 'Caltex';
        } else {
          $scope.currentGasStation = $scope.items[data.activeIndex].nameEn;
        }

        $scope.$apply();
      });

      $scope.goBack = function () {
        $ionicHistory.goBack();
      };
    }
  }
];
