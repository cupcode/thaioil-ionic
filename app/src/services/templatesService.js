'use strict';

/**
 * @ngdoc function
 * @name Thaioil.service:ExampleService
 * @description
 * # ExampleService
 */
module.exports = [
  function() {

    return {
      analysisCardList: 'templates/directiveViews/analysis/analysisCardList.html',
      analysisMediaList: 'templates/directiveViews/analysis/analysisMediaList.html',
      newsSlidePage: 'templates/directiveViews/news/newsSlidePage.html',
      breakingNewsCardList: 'templates/directiveViews/news/breakingNewsCardList.html',
      marketsCardList: 'templates/directiveViews/market/marketsCardList.html',
      calendarList: 'templates/directiveViews/calendar/calendarList.html',
      promotionCardList: 'templates/directiveViews/promotion/promotionCardList.html'
    };

  }
];
