'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:MainController
 * @description
 * # MainController
 */
module.exports = [
  '$scope', '$window', '$ionicFilterBar', '$state', '$ionicSlideBoxDelegate', 'allService','$rootScope','$location','$ionicHistory','$ionicPlatform','$ionicScrollDelegate',

  function($scope, $window, $ionicFilterBar, $state, $ionicSlideBoxDelegate, allService,$rootScope,$location, $ionicHistory, $ionicPlatform,$ionicScrollDelegate) {

    $ionicPlatform.ready(function() {

        $scope.$on("$ionicView.beforeEnter", function(event, data){
          if(ionic.Platform.isWebView()){
            screen.lockOrientation('portrait');
          }
        });

        $scope.lockSlide = function() {
          $ionicSlideBoxDelegate.enableSlide(false);
        };

        $scope.Width = $window.innerWidth - 20;
        $scope.Height = $window.innerHeight - (44 + 40 + 47);

        $scope.listHeight = {
          'height': $scope.Height + 'px'
        };
        $scope.setWidth = {
          'width': $scope.Width + 'px'
        };

        $scope.slideHasChanged = function(index){
          $rootScope.$broadcast('slideHasChanged',{index: index, resource: 'news'});
        };
        $scope.newsCount = $rootScope.newsCount;
        $rootScope.$on('newsCount', function(events, count){
           $scope.newsCount = $rootScope.newsCount;
        });

        function getNewsSources(){
          allService.search('newsSources', 'findAllActiveNewsSource', {})
          .then(function successCallback(response) {

              $scope.newsSources = [{
                id: -1,
                checked: true,
                name: 'ThaiOil',
                src: $scope.baseUrl + '/thaioil/logo.jpg'
              }];

              $scope.data = response.data._embedded.newsSources;
              var selectedSources = []; //initial by select all newsSources..
              $scope.data.forEach(function(src){
                  var tmp = {};
                  tmp.id = src.id;
                  tmp.checked = true;
                  if(src.logoPath){
                    tmp.src = $rootScope.baseUrl + src.logoPath;
                  }else{
                    tmp.src = null;
                  }
                  tmp.name  = src.name;
                  $scope.newsSources.push(tmp);
                  
                  selectedSources.push(tmp.id);
              });
              $rootScope.newsSources = $scope.newsSources;
              $scope.$broadcast('newsSourceIds', selectedSources.join());
              console.log("broadcast newsSourceIds ", selectedSources.join());
          }, function errorCallback(response) {
              console.log('Error list resource=' + $scope.resource,response);
          });
        }

        $scope.$on("$ionicView.afterEnter", function(event, data){
          $ionicHistory.clearHistory();
          if($rootScope.newsSources){
            $scope.newsSources = $rootScope.newsSources;
          }else{
            getNewsSources();
          }

        });

    });
  }
];
