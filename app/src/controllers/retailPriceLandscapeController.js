'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:SettingsController
 * @description
 * # SettingsController
 */
module.exports = [
  '$scope', '$ionicHistory', '$translate', '$rootScope', '$stateParams', 'allService', '$state', '$location', '$q',

  function ($scope, $ionicHistory, $translate, $rootScope, $stateParams, allService, $state, $location, $q) {
    {

      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.getGasStaionPriceList();
      });

      $scope.$on('orientationchange', function (event, args) {
        console.log('orientationchange:', args, event);
        $scope.getGasStaionPriceList();
        if ((args === 'portrait-primary' || args === 'portrait-secondary') && $location.path().indexOf('retailPriceLandscape') > -1) {
          $ionicHistory.goBack();
        }
      });

      $scope.resource = 'retailPrices';
      $scope.searchMethod = 'findLastestRetailPrice';

      $scope.getGasStaionPriceList = function () {
        $q.all([
          allService.search("petrolStationTypes", "findAllRetailPrice", ""),
          allService.search($scope.resource, $scope.searchMethod, ""),
          allService.getById("retailTaxes", 1),
        ]).then(function successCallback(response) {
          var orderTemp = response[0].data._embedded.petrolStationTypes;
          $scope.tax = parseFloat(response[2].data.tax);
          $scope.orderIndex = []
          var data = response[1].data;
          $scope.temp = data._embedded[$scope.resource];
          $scope.items = [];
          for (var i = 0; i < orderTemp.length; i++) {
            if (orderTemp[i].ref.toLowerCase() !== 'esso' && orderTemp[i].ref.toLowerCase() !== 'bangchak' && orderTemp[i].ref.toLowerCase() !== 'thaioil') {
              $scope.orderIndex[orderTemp[i].orderRetailPrice - 1] = orderTemp[i];
              for (var index = 0; index < $scope.temp.length; index++) {
                if (orderTemp[i].ref === $scope.temp[index].nameEn) {
                  $scope.items[orderTemp[i].orderRetailPrice - 1] = $scope.temp[index];
                }
              }
            }
          }
          $scope.items = _.compact($scope.items);
          $scope.orderIndex = _.compact($scope.orderIndex);
          for (var index = 0; index < $scope.items.length; index++) {
            console.log('$scope.items', $scope.items[index]);
            var dataTemp = $scope.items[index];
            $scope.items[index].diesel = dataTemp.diesel !== null ? parseFloat(dataTemp.diesel) + $scope.tax : dataTemp.diesel;
            $scope.items[index].gasohol91 = dataTemp.gasohol91 !== null ? parseFloat(dataTemp.gasohol91) + $scope.tax : dataTemp.gasohol91;
            $scope.items[index].gasohol95 = dataTemp.gasohol95 !== null ? parseFloat(dataTemp.gasohol95) + $scope.tax : dataTemp.gasohol95;
            $scope.items[index].gasoholE20 = dataTemp.gasoholE20 !== null ? parseFloat(dataTemp.gasoholE20) + $scope.tax : dataTemp.gasoholE20;
            $scope.items[index].gasoholE85 = dataTemp.gasoholE85 !== null ? parseFloat(dataTemp.gasoholE85) + $scope.tax : dataTemp.gasoholE85;
            $scope.items[index].gasoline95 = dataTemp.gasoline95 !== null ? parseFloat(dataTemp.gasoline95) + $scope.tax : dataTemp.gasoline95;
            $scope.items[index].premiumDiesel = dataTemp.premiumDiesel !== null ? parseFloat(dataTemp.premiumDiesel) + $scope.tax : dataTemp.premiumDiesel;

          }
          console.log('$scope.items gasStationPrice', $scope.items);

          $scope.icons = [];
          $scope.gasoline95 = {
            prices: []
          };
          $scope.gasohol95 = {
            prices: []
          };
          $scope.gasohol91 = {
            prices: []
          };
          $scope.gasoholE20 = {
            prices: []
          };
          $scope.gasoholE85 = {
            prices: []
          };
          $scope.diesel = {
            prices: []
          };
          $scope.premiumDiesel = {
            prices: []
          };

          for (var i = 0; i < $scope.items.length; i++) {

            if ($scope.items[i].nameEn !== 'iRPC' && $scope.items[i].nameEn !== 'RPC') {
              $scope.icons.push({
                nameEn: $scope.items[i].nameEn
              });
              $scope.gasoline95.prices.push({
                p: $scope.items[i].gasoline95
              });
              $scope.gasohol95.prices.push({
                p: $scope.items[i].gasohol95
              });
              $scope.gasohol91.prices.push({
                p: $scope.items[i].gasohol91
              });
              $scope.gasoholE20.prices.push({
                p: $scope.items[i].gasoholE20
              });
              $scope.gasoholE85.prices.push({
                p: $scope.items[i].gasoholE85
              });
              $scope.diesel.prices.push({
                p: $scope.items[i].diesel
              });
              $scope.premiumDiesel.prices.push({
                p: $scope.items[i].premiumDiesel
              });
            }
          }


        }, function errorCallback(response) {
          console.log('Error search resource:' + $scope.resource + ',searchMethod:' + $scope.searchMethod, response);
        });
        $scope.$broadcast('scroll.refreshComplete');
      };

      // $scope.refresh = function () {
      //   $q.all([
      //     allService.search("petrolStationTypes", "findAllRetailPrice", ""),
      //     allService.search($scope.resource, $scope.searchMethod, ""),
      //     allService.getById("retailTaxes", 1),
      //   ]).then(function successCallback(response) {

      //     $scope.serverUrl = JSON.parse(localStorage.getItem('server')).url + '/gasStations/logo/';
      //     console.log('$stateParams', $stateParams);
      //     $scope.items = $stateParams.item;
      //     $scope.tax = $stateParams.tax;
      //     $scope.icons = [];
      //     $scope.gasoline95 = {
      //       prices: []
      //     };
      //     $scope.gasohol95 = {
      //       prices: []
      //     };
      //     $scope.gasohol91 = {
      //       prices: []
      //     };
      //     $scope.gasoholE20 = {
      //       prices: []
      //     };
      //     $scope.gasoholE85 = {
      //       prices: []
      //     };
      //     $scope.diesel = {
      //       prices: []
      //     };
      //     $scope.premiumDiesel = {
      //       prices: []
      //     };

      //     for (var i = 0; i < $scope.items.length; i++) {

      //       if ($scope.items[i].nameEn !== 'iRPC' && $scope.items[i].nameEn !== 'RPC') {
      //         $scope.icons.push({
      //           nameEn: $scope.items[i].nameEn
      //         });
      //         $scope.gasoline95.prices.push({
      //           p: $scope.items[i].gasoline95
      //         });
      //         $scope.gasohol95.prices.push({
      //           p: $scope.items[i].gasohol95
      //         });
      //         $scope.gasohol91.prices.push({
      //           p: $scope.items[i].gasohol91
      //         });
      //         $scope.gasoholE20.prices.push({
      //           p: $scope.items[i].gasoholE20
      //         });
      //         $scope.gasoholE85.prices.push({
      //           p: $scope.items[i].gasoholE85
      //         });
      //         $scope.diesel.prices.push({
      //           p: $scope.items[i].diesel
      //         });
      //         $scope.premiumDiesel.prices.push({
      //           p: $scope.items[i].premiumDiesel
      //         });
      //       }
      //     }
      //   })
      // }

      $scope.getGasStaionPriceList();

    }
  }
];
