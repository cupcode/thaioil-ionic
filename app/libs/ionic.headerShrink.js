angular.module('ionic.ion.headerShrink', [])

.directive('headerShrink', function($document) {
  var fadeAmt;

  var shrink = function(header, content, amt, max) {
    amt = Math.min(max, amt);
    fadeAmt = 1 - amt / max;
    ionic.requestAnimationFrame(function() {
      header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, -' + amt + 'px, 0)';
      for(var i = 0, j = header.children.length; i < j; i++) {
        header.children[i].style.opacity = fadeAmt;
      }
    });
  };

  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

        $scope.$on("headerShrink",function(){

          var starty = $scope.$eval($attr.headerShrink) || 0;
          var shrinkAmt;

          var amt;

          var y = 0;
          var prevY = 0;
          var scrollDelay = 0.4;

          var fadeAmt;

          var header = $document[0].body.querySelector('.bar-header');
          var tabs = $document[0].body.querySelector('div.tabs');
          var content = $document[0].body.querySelector('.scroll-content');
          var headerHeight = header.offsetHeight;

          function onScroll(e) {

            var scrollTop = e.detail.scrollTop;

            if(scrollTop >= 0) {
              y = Math.min(headerHeight / scrollDelay, Math.max(0, y + scrollTop - prevY));
            } else {
              y = 0;
            }

            console.log(scrollTop);

            //HEADER
            if(scrollTop <= 44){
                if(prevY < scrollTop){
                  //scrolldown
                  $('.scroll-content').css("top", -scrollTop +'px');

                }
                else{
                  if(scrollTop > 0){
                    $('.scroll-content').css("top", (44-scrollTop) +'px');
                  }
                }
            }
            //TABBAR
            if(scrollTop <= 49){
              if(prevY < scrollTop){
                //scrolldown
                $('.scroll-content').css("bottom", -scrollTop +'px');
              }
              else{
                if(scrollTop > 0){
                  $('.scroll-content').css("bottom", (49-scrollTop) +'px');
                }
              }
            }
            if(scrollTop == 0){
              $('.scroll-content').css("top", '44px');
              $('.scroll-content').css("bottom", '49px');
            }

            ionic.requestAnimationFrame(function() {
              fadeAmt = 1 - (y / headerHeight);
              header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, ' + -y + 'px, 0)';
              tabs.style[ionic.CSS.TRANSFORM] = 'translate3d(0, ' + y + 'px, 0)';
              for(var i = 0, j = header.children.length; i < j; i++) {
                header.children[i].style.opacity = fadeAmt;
                tabs.children[i].style.opacity = fadeAmt;
              }
            });

            prevY = scrollTop;
          }

          $element.bind('scroll', onScroll);

      });

    }
  }
});
