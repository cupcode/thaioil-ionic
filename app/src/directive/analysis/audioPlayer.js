'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

    function()
    {
        return {
            templateUrl: 'templates/directiveViews/analysis/audioPlayer.html',
            restrict: 'E',
            scope: {
              title: '=',
              audio: '=',
              theme: '='
            },
            controller: ['$scope','$rootScope',function($scope, $rootScope){

                if($rootScope.audioSrc === $scope.audio && $rootScope.audioPlaying){
                  $scope.playing = true;
                }else{
                  $scope.playing = false;
                }

                $scope.play = function(){
                  $rootScope.$broadcast('audioPlaying',{ audioTitle: $scope.title,audioUrl: $scope.audio});
                  $scope.playing = true;
                };

                $scope.pause = function(){
                  $rootScope.$broadcast('audioPausing');
                  $scope.playing = false;
                };

                $rootScope.$on('mainAudioPlaying',function(event){
                    if($rootScope.audioSrc === $scope.audio) {
                      $scope.playing = true;
                    }else{
                      $scope.playing = false;
                    }
                });

                $rootScope.$on('mainAudioPausing',function(event){
                    if($rootScope.audioSrc === $scope.audio) {
                      $scope.playing = false;
                    }
                });

                $rootScope.$on('mainAudioEnded',function(event){
                    // if($rootScope.audioSrc === $scope.audio) {
                      $scope.playing = false;
                    // }
                });

            }]

        };
    }
];
