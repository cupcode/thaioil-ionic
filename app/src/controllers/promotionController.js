'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:PromotionController
 * @description
 * # PromotionController
 */
module.exports = [
  '$scope', '$ionicFilterBar', 'allService', '$q', '$rootScope', '$location', '$ionicSlideBoxDelegate', '$ionicHistory', '$ionicPlatform', '$ionicScrollDelegate', '$timeout',

  function ($scope, $ionicFilterBar, allService, $q, $rootScope, $location, $ionicSlideBoxDelegate, $ionicHistory, $ionicPlatform, $ionicScrollDelegate, $timeout) {
    $ionicPlatform.ready(function () {
      console.log('viewHistory', $ionicHistory.viewHistory());
      $scope.resource = 'promotion';
      $scope.itemActive = 0;
      $scope.loading = true;

      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (ionic.Platform.isWebView()) {
          screen.lockOrientation('portrait');
          console.log('lock portrait');
        }
        $scope.loadData()
      });
      // $scope.$on("$ionicView.afterEnter", function (event, data) {
      //   $scope.refresh()
      // });

      $scope.lockSlide = function () {
        $ionicSlideBoxDelegate.enableSlide(false);
      };

      $scope.slideHasChanged = function (index) {
        // $rootScope.$broadcast('slideHasChanged', { index: index, resource: 'promotionCategories' });
        console.log('index', index, $scope.categories[index])
        $scope.itemActive = index
        $ionicScrollDelegate.$getByHandle('content').resize();
        $ionicScrollDelegate.$getByHandle('content').scrollTop();
      };

      $scope.analysisCount = $rootScope.analysisCount;
      $rootScope.$on('analysisCount', function (events, count) {
        $scope.analysisCount = $rootScope.analysisCount;
      });
      $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
      $rootScope.$on('weeklyAnalysisCount', function (events, count) {
        $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
      });
      $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
      $rootScope.$on('specialAnalysisCount', function (events, count) {
        $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
      });

      var searchMethod = '';



      $scope.loadData = function () {
        console.log('refresh')
        $scope.loading = true;
        $q.all([
          allService.list('categories'),
        ])
          .then(function successCallback(results) {
            if (results[0].data._embedded) {
              $scope.categories = results[0].data._embedded.categories
              console.log('$scope.categories', $scope.categories)
              $scope.noContent = $scope.categories == undefined
              console.log('$scope.noContent', $scope.noContent)
              $scope.categories && $scope.categories.map(function (category, index) {
                var params = {
                  id: category.id,
                  page: 0,
                  name: '%%',
                  size: 6
                }
                $scope.categories[index].params = params
                allService.search('promotionCategories', 'listProductByCategories', params)
                  .then(function successCallback(response) {
                    $scope.categories[index].promotionList = response.data._embedded.promotions
                    $scope.categories[index].canload = ($scope.categories[index].params.page + 1) < response.data.page.totalPages
                    $scope.categories[index].params = _.assign({}, $scope.categories[index].params, response.data.page)
                    console.log('$scope.categories[', index, ']', $scope.categories[index])

                  }, function errorCallback(response) {
                    console.log('Error list resource=listProductByCategories', response);
                  });
              })
            }
            $scope.$broadcast('scroll.refreshComplete');
            $scope.loading = false;
          },
          function errorCallback(response) {
            console.log('Error search home', response);
          })
      }

      $scope.refresh = function () {
        $scope.loading = true;
        $scope.categories[$scope.itemActive].params.page = 0;

        allService.search('promotionCategories', 'listProductByCategories', $scope.categories[$scope.itemActive].params)
          .then(function successCallback(response) {
            $scope.categories[$scope.itemActive].promotionList = response.data._embedded.promotions
            $scope.categories[$scope.itemActive].canload = ($scope.categories[$scope.itemActive].params.page + 1) < response.data.page.totalPages
            $scope.categories[$scope.itemActive].params = _.assign({}, $scope.categories[$scope.itemActive].params, response.data.page)
            console.log('$scope.categories[', $scope.itemActive, ']', $scope.categories[$scope.itemActive])
            $scope.$broadcast('scroll.refreshComplete');
            $scope.loading = false;
          }, function errorCallback(response) {
            console.log('Error list resource=listProductByCategories', response);
          });
      }

      $scope.loadMore = function () {
        console.log('loadmore', $scope.categories[$scope.itemActive])
        $scope.categories[$scope.itemActive].params.page += 1

        allService.search('promotionCategories', 'listProductByCategories', $scope.categories[$scope.itemActive].params)
          .then(function successCallback(response) {
            $scope.categories[$scope.itemActive].promotionList = $scope.categories[$scope.itemActive].promotionList.concat(response.data._embedded.promotions)

            $scope.categories[$scope.itemActive].canload = ($scope.categories[$scope.itemActive].params.page + 1) < response.data.page.totalPages

            $scope.$broadcast('scroll.infiniteScrollComplete');
          }, function errorCallback(response) {
            console.log('Error list resource=listProductByCategories', response);
          });
      }

      $scope.$on('goBack', function () {
        console.log('on goBack promotion', $ionicHistory.viewHistory());
        $ionicHistory.goBack();

      });


    });
  }
];
