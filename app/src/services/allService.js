'use strict';

/**
 * @ngdoc function
 * @name Thaioil.service:ExampleService
 * @description
 * # ExampleService
 */
module.exports = ['$http', '$rootScope', '$q',
  function ($http, $rootScope, $q) {

    var allService = {};

    allService.registerNotification = function () {

      var registered = localStorage.getItem('registered');
      var registerId = localStorage.getItem('registerId');
      var deviceType = localStorage.getItem('deviceType');

      if (!registered) {
        if (registerId && deviceType) {
          console.log("First time use, regis default notification...")
          $q.all([
            allService.addNotification({
              deviceType: deviceType,
              deviceId: registerId,
              type: "NEWS"
            }),
            allService.addNotification({
              deviceType: deviceType,
              deviceId: registerId,
              type: "ANALYSIS"
            }),
            allService.addNotification({
              deviceType: deviceType,
              deviceId: registerId,
              type: "BREAKING_NEWS"
            })
          ]).then(function successCallback(results) {
            localStorage.setItem("NEWS", JSON.stringify(results[0].data));
            localStorage.setItem("ANALYSIS", JSON.stringify(results[1].data));
            localStorage.setItem("BREAKING_NEWS", JSON.stringify(results[2].data));
            localStorage.setItem("registered", true);
          }, function errorCallback(response) {
            console.log(response);
          });
        }
      }
      else {

        console.log("check notification...")

        if (registerId && deviceType) {

          var newsNoti = JSON.parse(localStorage.getItem("NEWS"));
          var analysisNoti = JSON.parse(localStorage.getItem("ANALYSIS"));
          var breakingNewsNoti = JSON.parse(localStorage.getItem("BREAKING_NEWS"));
          var calendarNoti = JSON.parse(localStorage.getItem("ECONOMIC_CALENDAR"));

          allService.checkSubscribe(registerId)
            .then(function successCallback(result) {

              var subscribeList = result.data;

              if (newsNoti) {

                var found = _.filter(subscribeList, function (sub) {
                  if (sub.type === 'NEWS')
                    return true;
                  else
                    return false;
                });

                if (found.length == 0) {

                  allService.addNotification({
                    deviceType: deviceType,
                    deviceId: registerId,
                    type: "NEWS"
                  })
                    .then(function successCallback(result) {
                      console.log("renoti NEWS success");
                      if ($rootScope.devMode) {
                        alert("re noti NEWS success!")
                      }
                      localStorage.setItem("NEWS", JSON.stringify(result.data));
                    }, function errorCallback(response) {
                      console.log(response);
                      if ($rootScope.devMode) {
                        alert("re noti NEWS fail!")
                      }

                    });
                } else {
                  //  if($rootScope.devMode){
                  //     alert("you already subscribe NEWS")
                  //   }
                }
              }
              if (analysisNoti) {

                var found = _.filter(subscribeList, function (sub) {
                  if (sub.type === 'ANALYSIS')
                    return true;
                  else
                    return false;
                });

                if (found.length == 0) {

                  allService.addNotification({
                    deviceType: deviceType,
                    deviceId: registerId,
                    type: "ANALYSIS"
                  })
                    .then(function successCallback(result) {
                      console.log("renoti ANALYSIS success");
                      if ($rootScope.devMode) {
                        alert("re noti ANALYSIS success!")
                      }
                      localStorage.setItem("ANALYSIS", JSON.stringify(result.data));
                    }, function errorCallback(response) {
                      console.log(response);
                      if ($rootScope.devMode) {
                        alert("re noti ANALYSIS fail!")
                      }
                    });
                } else {
                  //  if($rootScope.devMode){
                  //     alert("you already subscribe ANALYSIS")
                  //   }
                }
              }
              if (breakingNewsNoti) {

                var found = _.filter(subscribeList, function (sub) {
                  if (sub.type === 'BREAKING_NEWS')
                    return true;
                  else
                    return false;
                });

                if (found.length == 0) {

                  allService.addNotification({
                    deviceType: deviceType,
                    deviceId: registerId,
                    type: "BREAKING_NEWS"
                  })
                    .then(function successCallback(result) {
                      console.log("renoti BREAKING_NEWS success");
                      if ($rootScope.devMode) {
                        alert("re noti BREAKING_NEWS success!")
                      }
                      localStorage.setItem("BREAKING_NEWS", JSON.stringify(result.data));
                    }, function errorCallback(response) {
                      console.log(response);
                      if ($rootScope.devMode) {
                        alert("re noti BREAKING_NEWS fail!")
                      }
                    });
                } else {
                  //  if($rootScope.devMode){
                  //     alert("you already subscribe BREAKING_NEWS")
                  //   }
                }
              }
              if (calendarNoti) {

                var found = _.filter(subscribeList, function (sub) {
                  if (sub.type === 'ECONOMIC_CALENDAR')
                    return true;
                  else
                    return false;
                });

                if (found.length == 0) {

                  allService.addNotification({
                    deviceType: deviceType,
                    deviceId: registerId,
                    type: "ECONOMIC_CALENDAR"
                  })
                    .then(function successCallback(result) {
                      console.log("renoti ECONOMIC_CALENDAR success");
                      if ($rootScope.devMode) {
                        alert("re noti ECONOMIC_CALENDAR success!")
                      }
                      localStorage.setItem("ECONOMIC_CALENDAR", JSON.stringify(result.data));
                    }, function errorCallback(response) {
                      console.log(response);
                      if ($rootScope.devMode) {
                        alert("re noti ECONOMIC_CALENDAR fail!")
                      }

                    });
                } else {
                  //  if($rootScope.devMode){
                  //     alert("you already subscribe ECONOMIC_CALENDAR")
                  //   }
                }
              }

            }, function errorCallback(result) {
              //alert(JSON.stringify(result))
            });

        }
      }
    };

    allService.addNotification = function (data) {
      return $http({
        url: $rootScope.baseUrl + '/pushReceivers',
        method: 'POST',
        data: data
      });
    };

    allService.removeNotification = function (data) {
      return $http({
        url: $rootScope.baseUrl + '/pushReceivers/' + data.id,
        method: 'DELETE',
        data: data
      });
    };

    allService.get = function (resource, params) {
      return $http({
        url: $rootScope.baseUrl + '/' + resource,
        method: 'GET',
        params: params
      });
    };

    allService.list = function (resource, params) {

      return $http({
        url: $rootScope.baseUrl + '/' + resource,
        method: 'GET',
        params: params
      });

    };

    allService.allCalendarCountries = function () {
      return $http({
        url: $rootScope.baseUrl + '/countries',
        method: 'GET'
      });
    }

    allService.getActivePetrolStations = function () {
      return $http({
        url: $rootScope.baseUrl + '/getActivePetrolStations',
        method: 'GET'
      });
    }

    allService.countPromotion = function (id) {
      console.log('countPromotion', id)
      return $http({
        url: $rootScope.baseUrl + '/promotions/' + id + '/count',
        method: 'GET'
      });
    }

    allService.search = function (resource, searchMethod, params) {

      if (params && params.newsSourceIds && params.newsSourceIds.length > 0) {
        if (params.newsSourceIds.indexOf(-1) != -1) {
          searchMethod = searchMethod + 'AndNewsSourceIdAndThaiOil';
        }
        else if (params.newsSourceIds.indexOf(-1) == -1) {
          searchMethod = searchMethod + 'AndNewsSourceId';
        }
      }

      if (params && params.countries && params.countries.length > 0) {
        params.countryIds = params.countries
        delete params.countries
        searchMethod = searchMethod + 'AndCountryIds';
      }

      return $http({
        url: $rootScope.baseUrl + '/' + resource + '/search/' + searchMethod,
        method: 'GET',
        params: params
      });
    };

    //DETAIL
    allService.getById = function (resource, id) {
      return $http({
        url: $rootScope.baseUrl + '/' + resource + '/' + id,
        method: 'GET',
        params: {projection:'mobileView'}
      });
    };
    allService.viewCount = function (resource, id) {
      return $http({
        url: $rootScope.baseUrl + '/' + resource + '/' + id + '/view',
        method: 'GET'
      });
    };

    allService.getDetail = function (url) {
      return $http({
        url: url,
        method: 'GET',
        params: {projection:'mobileView'}
      });
    };

    allService.sendNotiAnalysis = function (registerId) {
      return $http({
        url: $rootScope.baseUrl + '/test/push/analysis/content/to/' + registerId,
        method: 'GET'
      });
    };
    allService.sendNotiBreakingNews = function (registerId) {
      return $http({
        url: $rootScope.baseUrl + '/test/push/breakingNews/content/to/' + registerId,
        method: 'GET'
      });
    };
    allService.sendNotification = function (message, registerId) {
      return $http({
        url: $rootScope.baseUrl + '/test/push/' + message + '/to/' + registerId + '/5',
        method: 'GET'
      });
    };
    allService.checkSubscribe = function (registerId) {
      return $http({
        url: $rootScope.baseUrl + '/test/subscription/' + registerId,
        method: 'GET'
      });
    };
    allService.getWelcomeSlides = function () {
      return $http({
        url: $rootScope.baseUrl + '/welcomeSlides/search/findExpire?projection=AdminList',
        method: 'GET'
      })
    }
    return allService;
  }
];
