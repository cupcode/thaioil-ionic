'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [

  '$scope', '$cordovaSocialSharing', '$ionicActionSheet', '$rootScope', '$state', '$ionicPlatform', '$timeout', '$ionicLoading', 'allService', '$ionicHistory',

  function ($scope, $cordovaSocialSharing, $ionicActionSheet, $rootScope, $state, $ionicPlatform, $timeout, $ionicLoading, allService, $ionicHistory) {

    $ionicPlatform.ready(function () {

      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (ionic.Platform.isWebView()) {
          screen.lockOrientation('portrait');
        }
      });

      $scope.openMap = function () {
        $state.go('map');
      };

      $scope.test = function () {
        $ionicActionSheet
          .show({
            buttons: [
              { text: '<b>Share</b> This' },
              { text: 'Move' }
            ],
            destructiveText: 'Delete',
            titleText: 'Modify your album',
            cancelText: 'Cancel',
            cancel: function () {
              // add cancel code..
            },
            buttonClicked: function (index) {
              console.log("index: ", index)
              window.open('https://www.google.com/maps/place/49.46800006494457,17.11514008755796', '_system')
              return true;
            }
          });
      }

      $scope.showActionSheet = function () {
        var message = 'ติดตามบทวิเคราะห์สถานการณ์ราคาน้ำมัน ข่าวสารเกี่ยวกับด้านน้ำมันแบบทันเหตุการณ์ สถานีน้ำมัน และราคาน้ำมันขายปลีกล่าสุด จาก TOP Energy Mobile Application โดย บริษัทไทยออยล์ จำกัด (มหาชน)';
        var subject = 'ThaiOil';
        var file = '';
        var link = '';

        if (ionic.Platform.isAndroid()) {
          link = 'https://play.google.com/store/apps/details?id=app.thaioilgroup.topenergy&hl=th';
        } else if (ionic.Platform.isIOS()) {
          link = 'https://itunes.apple.com/us/app/top-energy/id1158542320?ls=1&mt=8';
        }

        $cordovaSocialSharing.share(message, subject, file, link) // Share via native share sheet
          .then(function (result) {
            // Success!
          }, function (err) {
            // An error occured. Show a message to the user
          });

      };

      if (typeof cordova !== 'undefined') {
        cordova.getAppVersion.getVersionNumber(function (version) {
          $scope.version = version;
        });
      }

      var count = 0;
      $scope.showRegisterId = function () {
        count = count + 1;
        if (count == 7) {
          $rootScope.devMode = true;
          $scope.mode = true;
          $scope.registerId = localStorage.getItem('registerId');
          $scope.baseUrl = $rootScope.baseUrl; //just to show
          $scope.messageFromService = '';
        }
        else if (count > 7) {
          $rootScope.devMode = false;
          $scope.registerId = '';
          $scope.mode = false;
          $scope.messageFromService = '';
          count = 0;
        }

      }

      $scope.baseList = [
        {
          name: 'PRD',
          url: 'https://apitopenergy.thaioilgroup.com'
        },
        {
          name: 'QAS',
          url: 'https://apiqastopenergy.thaioilgroup.com'
        },
        {
          name: 'DEV',
          url: 'https://apidevtopenergy.thaioilgroup.com'
        },
        {
          name: 'GCP',
          url: 'http://35.201.165.54:9090'
        },
        {
          name: 'LOCAL',
          url: 'http://192.168.2.72:8080'
        }
      ];

      $scope.server = JSON.parse(localStorage.getItem('server'));

      $scope.changeBasedUrl = function () {

        var index = _.findLastIndex($scope.baseList, { url: $scope.baseUrl });

        index = index + 1;
        if (index === $scope.baseList.length) {
          index = 0;
        }
        $rootScope.baseUrl = $scope.baseList[index].url;
        $scope.baseUrl = $rootScope.baseUrl;
        $scope.server = $scope.baseList[index];
        localStorage.setItem('server', JSON.stringify($scope.server));

        allService.registerNotification();

        /*
          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();
          $timeout(function(){
            $rootScope.$broadcast('refresh','all');
          },2000);
        */

      }

      $scope.checkSubscribe = function () {
        $ionicLoading.show();
        allService.checkSubscribe($scope.registerId)
          .then(function successCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          }, function errorCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          });
      }
      $scope.sendNoti = function () {
        $ionicLoading.show();
        allService.sendNotification("HELLO!!", $scope.registerId)
          .then(function successCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          }, function errorCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          });
      }
      $scope.sendNotiBreakingNews = function () {
        $ionicLoading.show();
        allService.sendNotiBreakingNews($scope.registerId)
          .then(function successCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          }, function errorCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          });
      }
      $scope.sendNotiAnalysis = function () {
        $ionicLoading.show();
        allService.sendNotiAnalysis($scope.registerId)
          .then(function successCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          }, function errorCallback(result) {
            $ionicLoading.hide();
            $scope.messageFromService = result.data;
          });
      }


      $scope.shareRegisterId = function () {
        var message = localStorage.getItem('registerId');
        var subject = 'ThaiOil';
        var file = '';
        var link = '';

        $cordovaSocialSharing.share(message, subject, file, link) // Share via native share sheet
          .then(function (result) {
            // Success!
          }, function (err) {
            // An error occured. Show a message to the user
          });
      }

      $scope.promotionExist = $rootScope.promotionExist;
      $rootScope.$on('promotionExist', function (events, promotion) {
        $scope.promotionExist = promotion.promotion > 0;
      });

    });
  }
];
