'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */
module.exports = [
  function() {
    return {
      restrict: 'E',
      scope: {
        type: '@'
      },
      templateUrl: 'templates/directiveViews/news/breakingNewsCardList.html'
    };
  }
];
