'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = ['$sce','$rootScope',

    function($sce,$rootScope)
    {
        return {
            template: '<iframe id="video" src="{{trustSrc(video)}}" frameborder="0" width="100%" height="{{videoHeight}}" allowfullscreen></iframe>',
            // template: '<div id="video-placeholder"></div>',
            restrict: 'E',
            scope: {
              video: '=',
              videoId: '='
            },
            link: function(scope, elem, attr){

              scope.videoHeight = $rootScope.videoHeight;
              scope.trustSrc = function(src) {
                console.log('src:'+ src);
                return $sce.trustAsResourceUrl(src);
              };

              var url = scope.video.substring(scope.video.lastIndexOf('watch?v=')+8,scope.video.lastIndexOf('&feature'));
              scope.video = 'http://www.youtube.com/embed/' + url + '?rel=0&autoplay=1';

              // var player;
              //
              // function onYouTubeIframeAPIReady() {
              //     player = new YT.Player('video-placeholder', {
              //         width: 600,
              //         height: 400,
              //         videoId: url,
              //         // playerVars: {
              //             // color: 'white',
              //             // playlist: 'taJ60kskkns,FG0fTKAqZ5g'
              //         // },
              //         events: {
              //             onReady: initialize
              //         }
              //     });
              // }
              // function initialize(){
              //
              //     player.playVideo();
                  // Update the controls on load
                  //updateTimerDisplay();
                  //updateProgressBar();

                  // Clear any old interval.
                  //clearInterval(time_update_interval);

                  // Start interval to update elapsed time display and
                  // the elapsed part of the progress bar every second.
                  // time_update_interval = setInterval(function () {
                  //     updateTimerDisplay();
                  //     updateProgressBar();
                  // }, 1000)

              // }

            }
        };
    }
];
