'use strict';

/**
 * @ngdoc overview
 * @name Thaioil
 * @description
 * # Initializes main tablication and routing
 *
 * Main module of the tablication.
 */

// Example to require lodash
// This is resolved and bundled by browserify
//
// var _ = require( 'lodash' );

// https://github.com/tmaximini/generator-ionic-gulp#readme
angular.module('Thaioil', [
  'ionic',
  'ngCordova',
  'ngResource',
  'pascalprecht.translate',
  'tabSlideBox',
  'ion-gallery',
  'jett.ionic.filter.bar',
  'angularMoment',
  'dcbImgFallback',
  'jett.ionic.scroll.sista',
  'ionic.ion.headerShrink',
  'ngAnimate',
  'ngMap',
])

  .run(['$ionicPlatform', '$http', '$rootScope', '$state', '$cordovaPushV5', '$ionicPopup', '$translate', 'ConnectivityMonitor', '$cordovaDialogs', '$timeout',
    function ($ionicPlatform, $http, $rootScope, $state, $cordovaPushV5, $ionicPopup, $translate, ConnectivityMonitor, $cordovaDialogs, $timeout) {

      $ionicPlatform.ready(function () {

        ConnectivityMonitor.startWatching();

        window.addEventListener('orientationchange', function () {
          console.log(screen.orientation);
          if (screen.orientation.type) {
            $rootScope.$broadcast('orientationchange', screen.orientation.type);
          } else {
            $rootScope.$broadcast('orientationchange', screen.orientation);
          }
        });
        document.addEventListener('resume', onResume, false);

        function onResume() {
          $rootScope.$broadcast('refresh', 'all');
        }

        if (ionic.Platform.isWebView()) {
          screen.lockOrientation('portrait');
        }

        $rootScope.devMode = false;

        if (!localStorage.getItem('server')) {
          localStorage.setItem('server', JSON.stringify({
            name: 'PRD',
            url: 'https://apitopenergy.thaioilgroup.com'
          }));
          // localStorage.setItem('server', JSON.stringify({
          //   name: 'LOCAL',
          //   url: 'http://localhost:8080'
          // }));
          // localStorage.setItem('server', JSON.stringify({
          //   name: 'GCP',
          //   url: 'http://35.201.165.54:9090'
          // }));
          // localStorage.setItem('server', JSON.stringify({
          //     name: 'QAS',
          //     url: 'https://apiqastopenergy.thaioilgroup.com'
          // }));
        }
        $rootScope.baseUrl = JSON.parse(localStorage.getItem('server')).url;

        var isWebView = ionic.Platform.isWebView();
        var isIPad = ionic.Platform.isIPad();
        var isIOS = ionic.Platform.isIOS();
        var isAndroid = ionic.Platform.isAndroid();
        var isWindowsPhone = ionic.Platform.isWindowsPhone();

        var device = '';
        // if (isWebView) {
        //   device = 'webview';
        // }
        if (isAndroid) {
          device = 'Android';
        } else if (isIOS) {
          device = 'iOS';
        }
        //  else if (isIPad) {
        //   device = 'iOS';
        // } else if (isWindowsPhone) {
        //   device = 'windows';
        // }
        console.log('device: ', device);
        $http.defaults.headers.common['TG-Device-Type'] = device;
        localStorage.deviceType = device;
        /*
        * start within Platform ready
        */
        if (ionic.Platform.isWebView()) {

          $cordovaPushV5.initialize(  // important to initialize with the multidevice structure !!
            {
              android: {
                senderID: '46733837029',
                iconColor: '#0c5d9a',
                icon: 'ic_launcher'
              },
              ios: {
                alert: 'true',
                badge: 'true',
                sound: 'true'
              },
              windows: {}
            }
          ).then(function (result) {
            $cordovaPushV5.onNotification();
            $cordovaPushV5.onError();
            $cordovaPushV5.register().then(function (resultreg) {
              localStorage.registerId = resultreg;
              $rootScope.$broadcast('registerId');
            }, function (err) {
              // handle error
            });
          });
          /*
           * Push notification events
           */
          $rootScope.newsCount = 0;
          $rootScope.breakingNewsCount = 0;
          $rootScope.calendarCount = 0;
          $rootScope.analysisCount = 0;
          $rootScope.weeklyAnalysisCount = 0;
          $rootScope.specialAnalysisCount = 0;

          $cordovaPushV5.setBadgeNumber(0).then(function (result) {
            console.log('iOs setBadgeNumber then', result);
          }, function (err) {
            // handle error
          });
          $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, data) {  // use two variables here, event and data !!!

            if (data.additionalData.type === 'NEWS') {
              $rootScope.newsCount = $rootScope.newsCount + 1;
              $rootScope.$broadcast('newsCount');
              if (!data.additionalData.foreground) {
                $state.go('tab.news');
              }
            }
            else if (data.additionalData.type === 'BREAKING_NEWS') {
              $rootScope.breakingNewsCount = $rootScope.breakingNewsCount + 1;
              $rootScope.$broadcast('breakingNewsCount');
              if (!data.additionalData.foreground) {
                $state.go('tab.home');
              }
            }
            else if (data.additionalData.type === 'ECONOMIC_CALENDAR') {
              $rootScope.calendarCount = $rootScope.calendarCount + 1;
              $rootScope.$broadcast('calendarCount');
              if (!data.additionalData.foreground) {
                $rootScope.goToCalendar = true;
                $state.go('tab.home');
              }

            }
            else if (data.additionalData.type === 'ANALYSIS') {
              $rootScope.analysisCount = $rootScope.analysisCount + 1;
              $rootScope.$broadcast('analysisCount');
              if (!data.additionalData.foreground) {
                $state.go('tab.analysis');
              }

            }
            else if (data.additionalData.type === 'WEEKLY_ANALYSIS') {
              $rootScope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount + 1;
              $rootScope.$broadcast('weeklyAnalysisCount');
              if (!data.additionalData.foreground) {
                $state.go('tab.analysis');
              }

            }

            else if (data.additionalData.type === 'SPECIAL_ANALYSIS') {
              $rootScope.specialAnalysisCount = $rootScope.specialAnalysisCount + 1;
              if (!data.additionalData.foreground) {
                $state.go('tab.analysis');
              }
              $rootScope.$broadcast('specialAnalysisCount');
            }

            if (data.additionalData.foreground) {
              console.log('receive noti: ' + JSON.stringify(data));
              if ($rootScope.devMode) {
                alert('receive noti:' + JSON.stringify(data));
              }

            } else {
              console.log('receive noti background: ' + JSON.stringify(data));
              if ($rootScope.devMode) {
                alert('receive noti background: ' + JSON.stringify(data));
              }
            }

            $cordovaPushV5.finish().then(function (result) {
              //console.log('$cordovaPushV5.finish: ', result);
            }, function (err) {
              // handle error
            });
          });

          $rootScope.$on('$cordovaPushV5:errorOccurred', function (event, error) {
            console.log('$cordovaPushV5:errorOccurred', error);
            alert('cordovaPushV5 Error' + JSON.stringify(error));
          });
        }

        $state.go('tab.home');

        //fontSize config
        $rootScope.minFontSize = 16;
        $rootScope.maxFontSize = 20;
        $rootScope.fonts = [14, 16, 18, 20];
      });
    }
  ])
  .config(['$httpProvider', '$stateProvider', '$urlRouterProvider', '$translateProvider', '$ionicConfigProvider', '$provide',

    function ($httpProvider, $stateProvider, $urlRouterProvider, $translateProvider, $ionicConfigProvider, $provide) {

      moment.locale('th', {
        months: 'มกราคม_กุมภาพันธ์_มีนาคม_เมษายน_พฤษภาคม_มิถุนายน_กรกฏาคม_สิงหาคม_กันยายน_ตุลาคม_พฤศจิกายน_ธันวาคม'.split('_'),
        monthsShort: 'ม.ค._ก.พ._มี.ค._เม.ย._พ.ค._มิ.ย._ก.ค._ส.ค._ก.ย._ต.ค._พ.ย._ธ.ค.'.split('_'),
        weekdays: 'จันทร์_อังคาร_พุธ_พฤหัส_ศุกร์_เสาร์_อาทิตย์'.split('_'),
        weekdaysShort: 'จ._อ._พ._พฤ._ศ._ส._อา.'.split('_'),
        weekdaysMin: 'จ._อ._พ._พฤ._ศ._ส._อา.'.split('_'),
        longDateFormat: {
          LT: 'HH:mm',
          LTS: 'HH:mm:ss',
          L: 'DD/MM/YYYY',
          LL: 'D MMMM YYYY',
          LLL: 'D MMMM YYYY LT',
          LLLL: 'dddd D MMMM YYYY LT'
        },
        calendar: {
          sameDay: "[Aujourd'hui à] LT",
          nextDay: '[Demain à] LT',
          nextWeek: 'dddd [à] LT',
          lastDay: '[Hier à] LT',
          lastWeek: 'dddd [dernier à] LT',
          sameElse: 'L'
        },
        relativeTime: {
          future: 'ภายใน %s',
          past: '%sที่แล้ว',
          s: '%d วินาที',
          m: '1 นาที',
          mm: '%d นาที',
          h: '1 ชั่วโมง',
          hh: '%d ชั่วโมง',
          d: '1 วัน',
          dd: '%d วัน',
          M: '1 เดือน',
          MM: '%d เดือน',
          y: '1 ปี',
          yy: '%d ปี'
        },
        ordinalParse: /\d{1,2}(er|ème)/,
        ordinal: function (number) {
          return number + (number === 1 ? 'er' : 'ème');
        },
        meridiemParse: /AM|PM/,
        isPM: function (input) {
          return input.charAt(0) === 'M';
        },
        // in case the meridiem units are not separated around 12, then implement
        // this function (look at locale/id.js for an example)
        // meridiemHour : function (hour, meridiem) {
        //     return /* 0-23 hour, given meridiem token and hour 1-12 */
        // },
        meridiem: function (hours, minutes, isLower) {
          return hours < 12 ? 'AM' : 'PM';
        },
        week: {
          dow: 1, // Monday is the first day of the week.
          doy: 4  // The week that contains Jan 4th is the first week of the year.
        }
      });

      var language = JSON.parse(localStorage.getItem('language'));
      if (language) {
        $translateProvider.preferredLanguage(language);
        moment.locale(language);
      } else {
        $translateProvider.preferredLanguage('en');
        moment.locale('en');
      }

      // $provide.decorator('$exceptionHandler', ['$delegate', function ($delegate) {
      //   return function (exception, cause) {
      //     $delegate(exception, cause);

      //     // Decorating standard exception handling behaviour by sending exception to crashlytics plugin
      //     var message = exception.toString();
      //     // Here, I rely on stacktrace-js (http://www.stacktracejs.com/) to format exception stacktraces before
      //     // sending it to the native bridge
      //     var stacktrace = exception.stack.toLocaleString();
      //     navigator.crashlytics.logException('ERROR: ' + message + ', stacktrace: ' + stacktrace);
      //   };
      // }]);

      $stateProvider
        .state('tutorial', {
          url: '/tutorial',
          templateUrl: 'templates/views/tutorial.html',
          controller: 'TutorialController',
          params: {
            goto: null
          }
        })

        .state('tab', {
          url: '/tab',
          abstract: true,
          templateUrl: 'templates/main.html',
          controller: 'MainController'
        })

        .state('tab.home', {
          url: '/home',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/home.html',
              controller: 'HomeController'
            }
          },
          params: {
            goToCalendar: null
          }
        })
        .state('tab.homeNewsDetail', {
          url: '/home/news/detail/:itemId',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/newsDetail.html',
              controller: 'NewsDetailController'
            }
          },
          params: {
            item: null,
            resource: null,
            itemId: null,
            title: null
          }
        })
        .state('tab.homeAnalysisDetail', {
          url: '/home/analyses/detail/:itemId',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/analysisDetail.html',
              controller: 'AnalysisDetailController'
            }
          },
          params: {
            item: null,
            resource: null,
            itemId: null,
            homeMenu: null
          }
        })
        .state('tab.calendar', {
          url: '/home/calendar',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/calendar.html',
              controller: 'CalendarController'
            }
          }
        })
        .state('tab.promotions', {
          url: '/home/promotions',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/promotion.html',
              controller: 'PromotionController'
            }
          }
        })
        .state('tab.promotionDetail', {
          url: '/home/promotions/:itemId',
          cache: true,
          params: {
            item: null,
            resource: null,
            itemId: null,
            homeMenu: null
          },
          views: {
            'tab-home': {
              templateUrl: 'templates/views/promotionDetail.html',
              controller: 'PromotionDetailController'
            }
          }
        })
        .state('tab.gasStation', {
          url: '/home/gasStationPrice',
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/gasStationPrice.html',
              controller: 'GasStationPriceController'
            }
          }
        })
        .state('tab.retailPricePortrait', {
          url: '/home/retailPricePortrait',
          params: {
            items: null,
            index: null,
            orderIndex: null,
            tax: null
          },
          cache: true,
          views: {
            'tab-home': {
              templateUrl: 'templates/views/retailPricePortrait.html',
              controller: 'RetailPricePortraitController'
            }
          }
        })
        .state('retailPriceLandscape', {
          url: '/home/retailPriceLandscape',
          cache: true,
          params: {
            item: null,
            tax: null
          },
          templateUrl: 'templates/views/retailPriceLandscape.html',
          controller: 'RetailPriceLandscapeController'
        })
        .state('tab.analysis', {
          url: '/analyses',
          cache: true,
          views: {
            'tab-analysis': {
              templateUrl: 'templates/views/analysis.html',
              controller: 'AnalysisController'
            }
          }
        })
        .state('tab.analysisDetail', {
          url: '/analyses/detail/:itemId',
          params: {
            item: null,
            resource: null,
            itemId: null,
            homeMenu: null
          },
          cache: true,
          views: {
            'tab-analysis': {
              templateUrl: 'templates/views/analysisDetail.html',
              controller: 'AnalysisDetailController'
            }
          }
        })
        .state('analysisDetailLandscape', {
          url: '/analysisDetailLandscape',
          cache: false,
          params: {
            resource: null,
            item: null
          },
          templateUrl: 'templates/views/analysisDetailLandscape.html',
          controller: 'AnalysisDetailLandscapeController'
        })
        // .state('tab.weeklyDetail', {
        //   url: '/analyses/detail/:itemId',
        //   params: {
        //     item: null,
        //     resource: null,
        //     itemId: null
        //   },
        //   cache: true,
        //   views: {
        //     'tab-analysis': {
        //       templateUrl: 'templates/views/weeklyDetail.html',
        //       controller: 'AnalysisDetailController'
        //     }
        //   }
        // })
        .state('tab.news', {
          url: '/news',
          cache: true,
          views: {
            'tab-news': {
              templateUrl: 'templates/views/news.html',
              controller: 'NewsController'
            }
          }
        })
        .state('tab.newsDetail', {
          url: '/news/detail/:itemId',
          cache: true,
          views: {
            'tab-news': {
              templateUrl: 'templates/views/newsDetail.html',
              controller: 'NewsDetailController'
            }
          },
          params: {
            item: null,
            resource: null,
            itemId: null,
            title: null
          }
        })
        .state('newsDetailLandscape', {
          url: '/newsDetailLandscape',
          cache: false,
          params: {
            item: null
          },
          templateUrl: 'templates/views/newsDetailLandscape.html',
          controller: 'NewsDetailLandscapeController'
        })
        .state('tab.more', {
          url: '/more',
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/more.html',
              controller: 'MoreController'
            }
          }
        })
        .state('tab.moreCalendar', {
          url: '/moreCalendar',
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/calendar.html',
              controller: 'CalendarController'
            }
          }
        })
        .state('tab.moreGasStation', {
          url: '/more/gasStationPrice',
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/gasStationPrice.html',
              controller: 'GasStationPriceController'
            }
          }
        })
        .state('tab.settings', {
          url: '/more/settings',
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/settings.html',
              controller: 'SettingsController'
            }
          }
        })
        .state('tab.moreRetailPricePortrait', {
          url: '/more/retailPricePortrait',
          params: {
            items: null,
            index: null,
            tax: null
          },
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/retailPricePortrait.html',
              controller: 'RetailPricePortraitController'
            }
          }
        })
        .state('tab.disclaimer', {
          url: '/more/disclaimer',
          cache: true,
          views: {
            'tab-more': {
              templateUrl: 'templates/views/disclaimer.html',
              controller: 'DisclaimerController'
            }
          }
        })
        .state('tab.map', {
          url: '/map',
          cache: true,
          views: {
            'tab-map': {
              templateUrl: 'templates/views/map.html',
              controller: 'MapController'
            }
          }
        })
        ;
      $urlRouterProvider.otherwise('/tab/home');
      $ionicConfigProvider.tabs.style('standard');
      $ionicConfigProvider.tabs.position('bottom');

    }
  ])

  // Angular module controllers
  .config(require('./config/translateConfig'))

  .controller('MainController', require('./controllers/mainController'))
  .controller('HomeController', require('./controllers/homeController'))
  .controller('SettingsController', require('./controllers/settingsController'))
  
  .controller('MoreController', require('./controllers/moreController'))
  .controller('RetailPriceLandscapeController', require('./controllers/retailPriceLandscapeController'))
  .controller('RetailPricePortraitController', require('./controllers/retailPricePortraitController'))
  .controller('MapController', require('./controllers/mapController'))
  .controller('DisclaimerController', require('./controllers/disclaimerController'))
  .controller('TutorialController', require('./controllers/tutorialController'))
  .controller('GasStationPriceController', require('./controllers/gasStationPriceController'))
  .controller('CalendarController', require('./controllers/CalendarController'))

  .controller('PromotionController', require('./controllers/promotionController'))
  .controller('PromotionDetailController', require('./controllers/promotionDetailController'))

  .controller('NewsController', require('./controllers/newsController'))
  .controller('NewsDetailController', require('./controllers/newsDetailController'))
  .controller('NewsDetailLandscapeController', require('./controllers/newsDetailLandscapeController'))

  .controller('AnalysisController', require('./controllers/analysisController'))
  .controller('AnalysisDetailController', require('./controllers/analysisDetailController'))
  .controller('AnalysisDetailLandscapeController', require('./controllers/analysisDetailLandscapeController'))
  // Angular module services
  //
  .factory('ApiService', require('./services/ApiService'))
  .factory('allService', require('./services/allService'))
  .factory('templatesService', require('./services/templatesService'))
  .factory('ConnectivityMonitor', require('./services/connectivityMonitor'))
  // Angual

  //directives
  .directive('welcomeModal', require('./directive/welcomeModal'))
  .directive('singleCard', require('./directive/singleCard'))
  .directive('tripleCard', require('./directive/tripleCard'))
  .directive('ionSlideTabs', require('./directive/slidingTabs'))
  .directive('ionSlideTabLabel', require('./directive/ionSlideTabLabel'))
  .directive('slidePage', require('./directive/slidePage'))
  .directive('doubleCards', require('./directive/doubleCards'))
  .directive('searchHeader', require('./directive/searchHeader'))
  .directive('dateLocale', require('./directive/dateLocale'))
  .directive('specialGiftCarousel', require('./directive/specialGiftCarousel'))

  .directive('oilDirectionCard', require('./directive/home/oilDirectionCard'))
  .directive('economicCalendar', require('./directive/home/economicCalendar'))

  .directive('marketCard', require('./directive/market/marketCard'))

  .directive('newsCard', require('./directive/news/newsCard'))
  .directive('breakingNewsCardList', require('./directive/news/breakingNewsCardList'))

  .directive('videoCard', require('./directive/analysis/videoCard'))
  .directive('audioCard', require('./directive/analysis/audioCard'))
  .directive('audioPlayer', require('./directive/analysis/audioPlayer'))
  .directive('videoPlayer', require('./directive/analysis/videoPlayer'))
  .directive('cardWithBadge', require('./directive/analysis/cardWithBadge'))
  .directive('cardWithTable', require('./directive/analysis/cardWithTable'))
  .directive('oilpriceCard', require('./directive/analysis/oilpriceCard'))

  .directive('faqPage', require('./directive/analysis/faqPage'))

  .directive('focusMe', require('./directive/focusMe'))
  .directive('imageonload', require('./directive/imageonload'))
  .directive('goToTop', require('./directive/goToTop'))

  ;
