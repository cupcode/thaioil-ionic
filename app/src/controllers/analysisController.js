'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope','$ionicFilterBar','$rootScope','$location','$ionicSlideBoxDelegate','$ionicHistory','$ionicPlatform','$ionicScrollDelegate','$timeout',

  function( $scope , $ionicFilterBar, $rootScope, $location,$ionicSlideBoxDelegate, $ionicHistory, $ionicPlatform, $ionicScrollDelegate,$timeout)
  {
    $ionicPlatform.ready(function() {

        $scope.$on("$ionicView.beforeEnter", function(event, data){
          if(ionic.Platform.isWebView()){
            screen.lockOrientation('portrait');
            console.log('lock portrait');
          }

        });
        $scope.$on("$ionicView.afterEnter", function(event, data){
            $ionicHistory.clearHistory();
        });

        $scope.lockSlide = function() {
          $ionicSlideBoxDelegate.enableSlide(false);
        };

        $scope.slideHasChanged = function(index){
          $rootScope.$broadcast('slideHasChanged',{index: index, resource: 'analyses'});
        };

        $scope.analysisCount = $rootScope.analysisCount;
        $rootScope.$on('analysisCount', function(events, count){
           $scope.analysisCount = $rootScope.analysisCount;
        });
        $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
        $rootScope.$on('weeklyAnalysisCount', function(events, count){
           $scope.weeklyAnalysisCount = $rootScope.weeklyAnalysisCount;
        });
        $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
        $rootScope.$on('specialAnalysisCount', function(events, count){
           $scope.specialAnalysisCount = $rootScope.specialAnalysisCount;
        });

    });
  }
];
