'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

  function () {
    return {
      // template: '<span>{{title}}</span>',
      templateUrl: 'templates/directiveViews/dateLocale.html',
      restrict: 'E',
      scope: {
        date: '=',
        short: '=',
        timeago: '=',
      },
      link: function (scope, elem, attr) {

        scope.$watch('date', function () {
          
          var date = moment(scope.date)
          var dateFormat

          if (!date.isValid()) {
            date = moment(scope.date, 'DD-MM-YYYY, HH:mm')
            dateFormat = date.format('DD MMM YYYY')
            // console.log('invalid date', date)
          } else {
            dateFormat = date.format('DD MMM YYYY');
          }

          var dateSplit = dateFormat.split(' ');
          if (moment.locale() == 'th') {
            dateSplit[2] = parseInt(dateSplit[2]) + 543;
          }
          if (scope.short) {
            var test = dateSplit[2] + '';
            dateSplit[2] = test.substr(-2);
          }
          scope.title = dateSplit.join(' ');

          if (scope.timeago) {
            // console.log('date', date)
            scope.title = date.fromNow();
          }
        });

      }
    };
  }
];
