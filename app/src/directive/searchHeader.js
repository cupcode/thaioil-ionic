'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [

  function () {
    return {
      templateUrl: 'templates/directiveViews/searchHeader.html',
      restrict: 'E',
      scope: {
        title: '@',
        map: '=',
        filter: '=',
        canSearch: '=',
        internetStyleTop: '@',
        canBack: '=',
        sources: '=',
        filterName: '@',
        typeList: '=',
        disabledSearchEnter: '='
      },
      controller: ['$scope', '$rootScope', '$location', '$state', '$ionicModal', 'allService', '$timeout', '$ionicHistory', '$ionicPlatform', 'ConnectivityMonitor',
        function ($scope, $rootScope, $location, $state, $ionicModal, allService, $timeout, $ionicHistory, $ionicPlatform, ConnectivityMonitor) {

          $ionicPlatform.ready(function () {

            $scope.baseUrl = $rootScope.baseUrl;

            $scope.deviceType = localStorage.getItem('deviceType');
            $scope.server = JSON.parse(localStorage.getItem('server'));
            $scope.search = {};
            $scope.search.keyword = "";
            $scope.showSearchBar = false;

            $scope.goBack = function () {
              $rootScope.$broadcast('goBack');
              console.log('broadcast goBack');
            };

            $scope.cancel = function () {
              $scope.showSearchBar = false;
              $scope.search.keyword = "";
              $timeout(function () {
                if (!$scope.disabledSearchEnter) {
                  $rootScope.$broadcast('searchEnter', { keyword: null, location: $location.path() });
                }

              }, 200);
            };
            $scope.$watch('search.keyword', function () {

              $rootScope.$broadcast('search', {
                keyword: $scope.search.keyword,
                location: $location.path()
              });

              console.log('search:' + $scope.search.keyword);
            });
            $scope.searchEnter = function () {

              if (!$scope.disabledSearchEnter) {
                $rootScope.$broadcast('searchEnter', {
                  keyword: "%" + $scope.search.keyword + "%",
                  location: $location.path()
                });
              }

            };
            $scope.clearSearch = function () {
              $scope.search.keyword = "";
              $timeout(function () {
                $scope.$broadcast('searchActive');
              }, 100);
            };
            $scope.openMap = function () {
              $state.go('tab.map');
            };

            $ionicModal.fromTemplateUrl('templates/views/filter.html', {
              scope: $scope
            }).then(function (modal) {
              $scope.modal = modal;
            });

            $scope.showModal = function () {
              console.log('$scope.source', $scope.sources)
              $scope.modal.show();
            };

            $scope.checkAll = false;
            $scope.toggleCheckAll = function () {
              console.log($scope.checkAll);
              if ($scope.checkAll) {
                $scope.sources.forEach(function (src) {
                  src.checked = true;
                });
              } else {
                $scope.sources.forEach(function (src) {
                  src.checked = false;
                });
              }
              $scope.checkAll = !$scope.checkAll;
            };

            $rootScope.$on('setToggleAll', function (event, args) {
              console.log('on setToggleAll:' + args);
              $scope.checkAll = args;
            });

            var selectedSources = [];
            function prepareSources() {
              selectedSources = [];
              $scope.sources.forEach(function (src) {
                if (src.checked) {
                  selectedSources.push(src.id);
                }
              });
              $rootScope.$broadcast($scope.filterName, selectedSources.join());
              $rootScope[$scope.filterName] = selectedSources;
              console.log('searchHeader broadcast ' + $scope.filterName, selectedSources.join())
            }
            $scope.closeModal = function () {
              $scope.modal.hide();
            };
            // Execute action on hide modal
            $scope.$on('modal.hidden', function () {
              prepareSources();
            });

            $scope.searchActive = function () {
              $scope.showSearchBar = true;

              $timeout(function () {
                $scope.$broadcast('searchActive');
              }, 1000);
            };

            $scope.offline = ConnectivityMonitor.isOffline();

            $rootScope.$on('online', function (event, args) {
              $scope.offline = false;
            });
            $rootScope.$on('offline', function (event, args) {
              $scope.offline = true;
            });

          });

        }]
    };
  }
];
