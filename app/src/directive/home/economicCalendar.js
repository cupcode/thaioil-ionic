'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:newsCard
 * @description
 * # newsCard
 */
module.exports = [
  function() {
    return {
      restrict: 'E',
      scope: {
        title: '@',
        items: '='
      },
      templateUrl: 'templates/directiveViews/home/economicCalendar.html'
    };
  }
];
