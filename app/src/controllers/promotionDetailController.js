'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:PromotionDetailController
 * @description
 * # PromotionDetailController
 */
module.exports = [
  '$scope', 'allService', '$stateParams', '$ionicHistory', '$ionicPlatform', '$sce', '$rootScope',

  function ($scope, allService, $stateParams, $ionicHistory, $ionicPlatform, $sce, $rootScope) {
    $ionicPlatform.ready(function () {
      $scope.resource = $stateParams.resource;
      $scope.data = $stateParams.item;

      console.log('$scope.resource', $scope.resource)
      console.log('$scope.data', $scope.data)
      console.log('$scope.title', $scope.title)

      ////------------------------ GET SELF DATA ------------------------
      $scope.loading = true;
      allService.getById($scope.resource, $scope.data.id)
        .then(function successCallback(response) {
          $scope.item = response.data;
          // $scope.item.startDate = moment($scope.item.startDate, 'DD-MM-YYYY HH:mm').format('DD MMMM YYYY')
          // $scope.item.expireDate = moment($scope.item.expireDate, 'DD-MM-YYYY HH:mm').format('DD MMMM YYYY')
          // console.log('$scope.item:', $scope.item);

          $scope.loading = false;

          allService.countPromotion($scope.data.id)
            .then(function successCallback(resp) {
              console.log('countPromotion success')
            }, function errorCallback(resp) {
              console.log('Error: countPromotion ', resp);
            })

        }, function errorCallback(response) {
          console.log('Error: getById ' + $scope.resource + '/' + $scope.data.id, response);
        });

      $scope.trust = function (html_code) {
        return $sce.trustAsHtml(html_code);
      };

      $scope.goBack = function () {
        console.log('on goBack promotion', $ionicHistory.viewHistory());
        $ionicHistory.goBack();
      }

      if (localStorage.fontSize) {
        $scope.fontSize = parseInt(localStorage.fontSize);
      } else {
        $scope.fontSize = $rootScope.fonts[0];
      }

      $scope.toggleFont = function () {
        var index = _.indexOf($rootScope.fonts, $scope.fontSize);
        if (index === $rootScope.fonts.length - 1) {
          index = -1;
        }
        $scope.fontSize = $rootScope.fonts[index + 1];
        localStorage.fontSize = $scope.fontSize;
        $rootScope.$broadcast('fontSizeChanged');
      }

      $scope.onSwipeRight = function () {
        console.log('onSwipeRight')
        $scope.goBack();
      }

    });
  }
];
