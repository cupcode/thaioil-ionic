'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = [
    '$rootScope', '$interval',
    function ($rootScope, $interval) {
        return {
            templateUrl: 'templates/directiveViews/tripleCard.html',
            restrict: 'E',
            scope: {
                resource1: '=',
                resource2: '=',
                resource3: '=',
                resource4: '=',
                item1: '=',
                item2: '=',
                item3: '=',
                item4: '=',
                customClass: '=',
                mockLoading: "=",
                homeMenu: '='
            },
            link: function (scope, elem, attr) {

                // scope.serverUrl = 'https://apitopenergy.thaioilgroup.com';
                // scope.serverUrl = 'http://35.201.165.54:9090';
                scope.serverUrl = $rootScope.baseUrl;
                console.log('scope.serverUrl', scope.serverUrl)

                scope.setServer = $interval(function () {
                    if (!scope.serverUrl) {
                        scope.serverUrl = $rootScope.baseUrl

                        scope.defaultImage1 = setDefaultImage(scope.resource1)
                        scope.defaultImage2 = setDefaultImage(scope.resource2)
                        scope.defaultImage3 = setDefaultImage(scope.resource3)
                        scope.defaultImage4 = setDefaultImage(scope.resource4)
                    } else {
                        scope.stopFight();
                    }
                    console.log('interval')
                }, 100);

                scope.stopFight = function () {
                    if (angular.isDefined(scope.setServer)) {
                        $interval.cancel(scope.setServer);
                        scope.setServer = undefined;
                        console.log('scope.serverUrl', scope.serverUrl)
                    }
                };

                // scope.serverUrl = 'http://localhost:8080';
                // function imageExists(url, callback) {
                //     var img = new Image();
                //     img.onload = function() { callback(true); };
                //     img.onerror = function() { callback(false); };
                //     img.src = url;
                // }
                // scope.photoReady = false
                // scope.$watch('item1',function(){
                //     if(scope.item1 && scope.item1.thumbnailPath) {
                //         console.log("thumbnailPath: " , scope.item1.thumbnailPath)
                //         imageExists(scope.serverUrl + scope.item1.thumbnailPath, function(exists) {
                //           if(exists){
                //               scope.photoReady = true
                //               console.log("is exist " + scope.photoReady + " " + scope.item1.thumbnailPath)
                //           }else{
                //  scope.item1.thumbnailPath = '/analysis/default/daily.png'
                //               scope.photoReady = true
                //               console.log("is not exist " + scope.photoReady + " "  + scope.item1.thumbnailPath)
                //           }
                //         })
                // }
                //     else if (scope.item1 && !scope.item1.thumbnailPath){
                //         scope.photoReady = true
                //     }
                // });

                function rename(resource) {
                    switch (resource) {
                        case "analyses": return "Daily Analysis ";
                        case "weeklyAnalyses": return "Weekly Analysis ";
                        case "specialAnalyses": return "Special Analysis ";
                    }
                }
                scope.titlePlayer1 = scope.resource1 ? rename(scope.resource1) + moment(scope.resource1.timeago).format('DD MMM YYYY') : '';
                scope.titlePlayer2 = scope.resource2 ? rename(scope.resource2) + moment(scope.resource2.timeago).format('DD MMM YYYY') : '';
                scope.titlePlayer3 = scope.resource3 ? rename(scope.resource3) + moment(scope.resource3.timeago).format('DD MMM YYYY') : '';
                scope.titlePlayer4 = scope.resource4 ? rename(scope.resource4) + moment(scope.resource4.timeago).format('DD MMM YYYY') : '';


                function setDefaultImage(resource) {
                    switch (resource) {
                        case "analyses":
                            return scope.serverUrl + '/analysis/default/daily.png'
                        case "weeklyAnalyses":
                            return scope.serverUrl + '/analysis/default/weekly.png'
                        case "specialAnalyses":
                            return scope.serverUrl + '/analysis/default/special.png'
                    }
                }

            }
        };
    }
];
