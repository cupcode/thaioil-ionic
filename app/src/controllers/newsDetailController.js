'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:HomeController
 * @description
 * # HomeController
 */
module.exports = [
  '$scope', '$ionicHistory', '$stateParams', 'allService', '$timeout', '$rootScope', '$state', '$sce', '$location',

  function ($scope, $ionicHistory, $stateParams, allService, $timeout, $rootScope, $state, $sce, $location) {

    $scope.resource = $stateParams.resource;
    $scope.item = $stateParams.item;
    $scope.title = $stateParams.title;

    if (localStorage.fontSize) {
      $scope.fontSize = parseInt(localStorage.fontSize);
    } else {
      $scope.fontSize = $rootScope.fonts[0];
    }

    $scope.toggleFont = function () {
      var index = _.indexOf($rootScope.fonts, $scope.fontSize);
      if (index === $rootScope.fonts.length - 1) {
        index = -1;
      }
      $scope.fontSize = $rootScope.fonts[index + 1];
      localStorage.fontSize = $scope.fontSize;
      $rootScope.$broadcast('fontSizeChanged');
    }

    $scope.$on("$ionicView.afterEnter", function (event, data) {
      if (ionic.Platform.isWebView()) {
        screen.unlockOrientation();
        console.log('afterEnter newsDetail unlock');
      }
    });
    $scope.$on('orientationchange', function (event, args) {
      if ((args === 'landscape-primary' || args === 'landscape-secondary') && $location.path().indexOf('news/detail') > -1) {
        $state.go('newsDetailLandscape', { item: $scope.item });
      }
    });

    $scope.goBack = function () {
      $ionicHistory.goBack();
    };

    console.log('NEWS DETAIL:', $scope.id, $scope.item);
    $scope.newsSourceName = $scope.item.newsSourceName;
    $scope.baseUrl = $rootScope.baseUrl;

    if ($scope.resource === null) {
      $scope.resource = 'news';
    }

    //------------------------ MORE NEWS BY NEWSSOURCE ------------------------
    $scope.searchMethod = 'findSinceByNewsSource';
    $scope.params = {
      'page': 0, //always be 0
      'size': 3,
      'sort': 'publishDatetime,DESC',
      'since': $scope.item.publishDatetime.replace('T', ' ').split('.')[0],
      'newsSourceIds': $scope.item.newsSourceId,
      'projection': 'mobileList'
    };

    $scope.moreNews = [];
    allService.search($scope.resource, $scope.searchMethod, $scope.params)
      .then(function successCallback(response) {

        console.log('search moreNews:', response);
        var data = response.data;

        if (data.page && data.page.number < data.page.totalPages) {
          $scope.moreNews = data._embedded[$scope.resource];
        }

      }, function errorCallback(response) {
        console.log('Error list resource=' + $scope.resource, response);
      });

    ////------------------------ GET SELF DATA ------------------------
    $scope.loading = true;
    allService.getById($scope.resource, $scope.item.id)
      .then(function successCallback(response) {

        $scope.item = response.data;
        console.log('$scope.item:', $scope.item);
        $scope.loading = false;

        allService.viewCount($scope.resource, $scope.item.id)
          .then(function successCallback(response) {

            console.log("viewCount : ", response);

          }, function errorCallback(response) {


          })

      }, function errorCallback(response) {
        console.log('Error: getById ' + $scope.resource + '/' + $scope.item.id, response);
      });

    $scope.gotoNewsDetail = function (news) {

      if ($location.path().indexOf('home') > -1) {
        $state.go('tab.homeNewsDetail', { itemId: news.id, item: news, resource: $scope.resource, title: $scope.title });
      } else {
        $state.go('tab.newsDetail', { itemId: news.id, item: news, resource: $scope.resource, title: $scope.title });
      }

    };

    $scope.trust = function (html_code) {
      return $sce.trustAsHtml(html_code);
    };


    $scope.redirectImage = function (data) {
      if (data && data.newsSourceName === 'TNN24') {
        var dataSplit = data.ogImage.split("/")
        var result = ''
        for (var i = 0; i < dataSplit.length; i++) {
          if (i === dataSplit.length - 1) {
            result = result + '/' + dataSplit[i]
          } else if (i === 0) {
            result = dataSplit[i]
          } else {
            result = result + '/' + dataSplit[i]
          }
        }
        // console.log('redirectImage', data, result)
        return result
      } else {
        return 'images/img/no_image300x300.png'
      }
    }

  }
];
