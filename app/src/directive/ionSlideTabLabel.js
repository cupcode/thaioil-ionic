'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:ionSlideTabLabel
 * @description
 * # ionSlideTabLabel
 */
module.exports = [function() {
  return {
    require: '^ionSlideTabs',
    link: function($scope, $element, $attrs, $parent) {
      $parent.addTab($attrs.ionSlideTabLabel);
    }
  };
}];
