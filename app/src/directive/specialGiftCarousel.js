'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = ['$rootScope',
    function ($rootScope) {
       
        return {
            templateUrl: 'templates/directiveViews/specialGiftCarousel.html',
            restrict: 'E',
            scope: {
                data: '=',
            },
            link: function (scope, elem, attr) {
                scope.serverUrl = JSON.parse(localStorage.getItem('server')).url
            }
        };
    }
];
