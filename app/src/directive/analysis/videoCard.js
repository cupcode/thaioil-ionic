'use strict';

/**
 * @ngdoc function
 * @name Thaioil.directive:marketCard
 * @description
 * # marketCard
 */
module.exports = ['$translate', '$cordovaInAppBrowser', '$rootScope',

  function ($translate, $cordovaInAppBrowser, $rootScope) {
    return {
      templateUrl: 'templates/directiveViews/analysis/videoCard.html',
      restrict: 'E',
      scope: {
        data: '='
      },
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.baseUrl = $rootScope.baseUrl;
        $scope.videoId = $scope.data.path.substring($scope.data.path.lastIndexOf('watch?v=') + 8, $scope.data.path.lastIndexOf('&feature'));
        console.log('$scope.videoId', $scope.videoId);
        $scope.linkVideo = 'http://img.youtube.com/vi/' + $scope.videoId + '/mqdefault.jpg';
        console.log('$scope.linkVideo', $scope.linkVideo);

        $scope.language = $translate.use();

        $scope.openWindow = function () {
          console.log($scope.data.path);
          window.open($scope.data.path, '_system');

          $cordovaInAppBrowser.open($scope.data.path, '_system', options)
            .then(function (event) {
              // success
            })
            .catch(function (event) {
              // error
            });

        }

      }]
    };
  }
];
