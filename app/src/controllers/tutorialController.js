'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:SettingsController
 * @description
 * # SettingsController
 */
module.exports = [
  '$scope', '$state','$ionicHistory','$stateParams','$translate','$q','$cordovaDialogs','allService','$ionicModal',

  function($scope, $state, $ionicHistory,$stateParams,$translate,$q,$cordovaDialogs,allService,$ionicModal) {
    {

      var tutorial = localStorage.getItem('tutorial');
      if(!tutorial){

        $ionicModal.fromTemplateUrl('templates/views/disclaimerModal.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.closeModal = function(){
          $scope.modal.hide();  
        };
        $scope.acceptAgreement = function(){
          $scope.modal.hide();
          $state.go('tab.home');
          localStorage.setItem('tutorial', true);
        };

      }

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        if(ionic.Platform.isWebView()){
          screen.lockOrientation('portrait');
          console.log('lock portrait');
        }

      });

      var goto = $stateParams.goto;

      $scope.continue = function(){

        if(goto === 'back'){
          $ionicHistory.goBack();
        }else{
          $scope.modal.show();
        }

      };

    }
  }
];
