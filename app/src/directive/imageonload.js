

    'use strict';

    /**
     * @ngdoc function
     * @name Thaioil.directive:marketCard
     * @description
     * # marketCard
     */
    module.exports = ['$rootScope',

        function($rootScope)
        {
          return {
              restrict: 'A',
              link: function(scope, element, attrs) {
                  element.bind('load', function() {
                      // console.log(attrs.id + " has loaded");
                      var height = $('#' + attrs.id).height();
                      $rootScope.videoHeight = height;
                  });
                  element.bind('error', function(){
                      // alert('image could not be loaded');
                  });
              }
          };
        }
    ];
