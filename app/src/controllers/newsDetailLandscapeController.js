'use strict';

/**
 * @ngdoc function
 * @name Thaioil.controller:MainController
 * @description
 * # MainController
 */
module.exports = [
  '$scope','$ionicHistory','$stateParams','$location','$rootScope',

  function($scope,$ionicHistory,$stateParams,$location,$rootScope) {

    $scope.item = $stateParams.item;

    if(localStorage.fontSize){
      $scope.fontSize = parseInt(localStorage.fontSize);
    }else{
      $scope.fontSize = $rootScope.minFontSize;
    }

    $scope.$on('orientationchange', function(event, args) {
      console.log('orientationchange:', args);
      if ((args === 'portrait-primary' || args === 'portrait-secondary') && $location.path().indexOf('newsDetailLandscape') > -1) {
        $ionicHistory.goBack();
      }
    });

  }
];
